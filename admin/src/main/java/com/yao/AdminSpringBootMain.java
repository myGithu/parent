package com.yao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * Created by ruijie on 2017/12/20.
 */
@SpringBootApplication
public class AdminSpringBootMain {
    public static void main(String[] args) {
        SpringApplication.run(AdminSpringBootMain.class,args);
    }
}
