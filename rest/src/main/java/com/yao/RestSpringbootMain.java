package com.yao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by ruijie on 2017/12/19.
 */
@SpringBootApplication
public class RestSpringbootMain {

    public static void main(String[] args) {
        SpringApplication.run(RestSpringbootMain.class,args);
    }
}
