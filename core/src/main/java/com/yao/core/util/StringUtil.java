package com.yao.core.util;

/**
 * Created by ruijie on 2017/12/22.
 */
public class StringUtil {
    private StringUtil(){}

    public static String getTabelNo(){
        StringBuilder tabelNo = new StringBuilder(DateTimeUtil.getYear());
        tabelNo.append(getWorkId());
        return tabelNo.toString();
    }

    public static String getWorkId(){
        return String.valueOf(System.currentTimeMillis()).substring(5);
    }
}
