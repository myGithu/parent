package com.yao.core.util;

import com.yao.core.annotation.EnumAsJavaBean;
import org.reflections.Reflections;

import java.lang.annotation.Annotation;
import java.util.*;

/**
 * Created by ruijie on 2018/1/27.
 */
public class AnnoManageUtil {

    public static Class<?>[] getAnnotationBean(Class<? extends Annotation> annotation,String... packageName) {
        if (Objects.isNull(packageName) || Objects.isNull(annotation)) {
            throw new NullPointerException("参数不能为空");
        }
        Reflections reflections = new Reflections(packageName);
        Set<Class<?>> classSet = reflections.getTypesAnnotatedWith(annotation);
        Class<?>[] enumArray = new Class[classSet.size()];
        return classSet.toArray(enumArray);
    }

    public static Class<? extends Enum>[] getEnumAsJavaBean(){
        return (Class<? extends Enum>[]) getAnnotationBean(EnumAsJavaBean.class,"com.yao.core.result");
    }

    public static void main(String[] args) {
        System.out.println(getAnnotationBean(EnumAsJavaBean.class,"com.yao.core.result","com.yao.core.properties.Test")[0] + ":" + getAnnotationBean(EnumAsJavaBean.class,"com.yao.core.result","com.yao.core.properties.Test")[1] );

    }

}
