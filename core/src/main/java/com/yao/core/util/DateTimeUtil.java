package com.yao.core.util;

import org.springframework.util.StringUtils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by ruijie on 2017/12/21.
 */
public class DateTimeUtil {
    private DateTimeUtil() {
    }

    public static String getStringNowDateYMD() {
        LocalDate date = LocalDate.now();
        return date.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    }

    public static String getStringNowDateShort() {
        return LocalDate.now().toString();
    }

    public static LocalDate getNowDateShort() {
        return LocalDate.now();
    }

    public static String getYear(){
        return String.valueOf(LocalDate.now().getYear());
    }

    public static String getStringNowDateTime() {
        return LocalDateTime.now().withNano(0).toString();
    }

    public static LocalDateTime getNowDateTime() {
        return LocalDateTime.now().withNano(0);
    }

    public static String getTimestamp() {
        return Instant.now().toString();
    }

    public static String getStringFormatDateTime(LocalDateTime localDateTime, String format) {
        if (StringUtils.isEmpty(format)) {
            format = "yyyy-MM-dd HH:mm:ss";
        }

        return localDateTime.format(DateTimeFormatter.ofPattern(format));
    }

    public static LocalTime getNowTime() {
        return LocalTime.now().withNano(0);
    }

    public static String getStringNowTime() {
        return LocalTime.now().withNano(0).toString();
    }

    public static void main(String[] args) {
        System.out.println(getStringNowTime());
    }
}
