package com.yao.core.util.sql;

/**
 * Created by ruijie on 2018/2/7.
 */
public enum SqlCRUD {
    INSERT("Insert"), UPDATE("Update"), DELETE("Delete"), SELECT("Select");

    private String crudValue;

    SqlCRUD(String crudValue) {
        this.crudValue = crudValue;
    }

    public String getCrudValue() {
        return crudValue;
    }
}
