package com.yao.core.util;


import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.yao.core.generator.mybatisplus.config.MyBatisPlusGeneratorConfig;
import com.yao.core.generator.mybatisplus.config.MybatisPlusStrategyConfig;

/**
 * Created by ruijie on 2018/1/30.
 */
public class ContextGeneratorUtil {


    public static void createMyBatisPlusGenerator(MyBatisPlusGeneratorConfig myBatisPlusGeneratorConfig) {
        new AutoGenerator().setDataSource(myBatisPlusGeneratorConfig.getDataSourceConfig())
                .setGlobalConfig(myBatisPlusGeneratorConfig.getGlobalConfig())
                .setPackageInfo(myBatisPlusGeneratorConfig.getPackageConfig())
                .setStrategy(myBatisPlusGeneratorConfig.getStrategyConfig())
                .setCfg(myBatisPlusGeneratorConfig.getInjectionConfig())
                .execute();
    }

    public static void main(String[] args) {
        createMyBatisPlusGenerator(
                new MyBatisPlusGeneratorConfig()
                        .setStrategyConfig(MybatisPlusStrategyConfig
                                .createStrategyConfig(new MybatisPlusStrategyConfig()
                                        .setInclude(new String[]{"schedule"}))));
    }


}
