package com.yao.core.util.sql;


import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by ruijie on 2018/1/18.
 */

public class CreateSql {
    private String tableName;

    private SqlCRUD sqlcrud = SqlCRUD.INSERT;

    private String sourcePath = DEFAULT_SOURCE_PATH + DEFAULT_SOURCE_FILE_SUFFIX;

    private String outPath;

    private String[] columnName = DEFAULT_COLUMNNAME;

    private String charset = DEFAULT_CHARSET;

    private String split = DEFAULT_SPLIT;

    private static final String DEFAULT_SPLIT = "\\s+";//空格

    private static final String DEFAULT_CHARSET = "UTF-8";

    private static final String DEFAULT_SOURCE_PATH = "C:\\Users\\ruijie\\Desktop\\";

    private static final String DEFAULT_OUT_PATH = "C:\\Users\\ruijie\\Desktop\\";

    private static final String[] DEFAULT_COLUMNNAME = {"yx_empId", "yx_empNo", "yx_empName"};

    private static final String DEFAULT_SOURCE_FILE_SUFFIX = "data.txt";

    private static List<String> content = new ArrayList<>();


    public CreateSql setTableName(String tableName) {
        this.tableName = tableName;
        return this;
    }

    public CreateSql setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath + DEFAULT_SOURCE_FILE_SUFFIX;
        return this;
    }

    public CreateSql setOutPath(String outPath) {
        this.outPath = checkedSqlCrud(outPath);
        return this;
    }

    public CreateSql setColumnName(String[] columnName) {
        this.columnName = columnName;
        return this;
    }

    public CreateSql setCharset(String charset) {
        this.charset = charset;
        return this;
    }

    public CreateSql setSplit(String split) {
        this.split = split;
        return this;
    }

    public String getTableName() {
        return tableName;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public String getOutPath() {
        return outPath;
    }

    public String[] getColumnName() {
        return columnName;
    }

    public String getCharset() {
        return charset;
    }

    public String getSplit() {
        return split;
    }

    public CreateSql(String tableName) {
        this.tableName = tableName;
        this.outPath = checkedSqlCrud(DEFAULT_OUT_PATH);
    }

    public CreateSql(String tableName, String sourcePath, String outPath, String[] columnName, String charset, String split) {
        this.tableName = tableName;
        this.sourcePath = sourcePath;
        this.outPath = outPath;
        this.columnName = columnName;
        this.charset = charset;
        this.split = split;
    }


    private String checkedSqlCrud(String outPath) {
        String _outPath = outPath + tableName + "CreateSql.sql";
        if (sqlcrud == null) {
            return _outPath;
        }
        switch (sqlcrud) {
            case DELETE:
                _outPath = outPath + tableName + SqlCRUD.DELETE.getCrudValue() + ".sql";
                break;
            case INSERT:
                _outPath = outPath + tableName + SqlCRUD.INSERT.getCrudValue() + ".sql";
                break;
            case SELECT:
                _outPath = outPath + tableName + SqlCRUD.SELECT.getCrudValue() + ".sql";
                break;
            case UPDATE:
                _outPath = outPath + tableName + SqlCRUD.UPDATE.getCrudValue() + ".sql";
                break;
            default:
                return _outPath;
        }
        return _outPath;
    }

    private void readFile(SqlCRUD sqlcrud) {

        try (BufferedReader read = new BufferedReader(new InputStreamReader(new FileInputStream(sourcePath), charset))) {
            String line = "";
            String[] arrs = null;
            while ((line = read.readLine()) != null) {
                arrs = line.split(split);
                switch (sqlcrud) {
                    case DELETE:
                        break;
                    case INSERT:
                        content.add(createInsertSql(arrs));
                        break;
                    case SELECT:
                        break;
                    case UPDATE:

                        break;
                    default:
                        throw new RuntimeException("请传入有效的参数标识！");
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private String createInsertSql(String[] values) {
        StringBuilder sql = new StringBuilder("INSERT ");
        sql.append(tableName);
        sql = clearArrayLastCommaToStringFromColumn(columnName, sql);
        sql.append(" VALUES ");
        sql = clearArrayLastCommaToStringFromValues(values, sql);
        return sql.toString();
    }

    private StringBuilder clearArrayLastCommaToStringFromColumn(String[] columns, StringBuilder sql) {
        sql.append(" (");
        for (int i = 0, j = columns.length; i < j; i++) {
            sql.append(columns[i]);
            if (i == columns.length - 1) {
                sql.append(")");
                break;
            }
            sql.append(",").append(" ");
        }
        return sql;
    }

    private StringBuilder clearArrayLastCommaToStringFromValues(String[] values, StringBuilder sql) {
        sql.append("(");
        for (int i = 0, j = values.length; i < j; i++) {
            sql.append("'").append(values[i]).append("'");
            if (i == values.length - 1) {
                sql.append(")");
                break;
            }
            sql.append(",").append(" ");
        }
        sql.append(";");
        return sql;
    }

    private void writerFile() {
        Objects.requireNonNull(content);
        try (final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outPath), charset))) {
            content.forEach(s -> {
                try {
                    writer.write(s + "\t\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void craeteSqlFile(SqlCRUD sqlcrud) {
        this.sqlcrud = sqlcrud;
        readFile(sqlcrud);
        writerFile();
    }

    public static void main(String[] args) {
        String[] userColumnNames = {"deptNo","userNo","userName","userSex","userTel","createTime"};

        String[] deptColumnNames = {"deptNo","userNo","userName","userSex","userTel","createTime"};
        String[] roleColumnNames = {"roleName"};
        String[] user_roleColumnNames = {"userId","roleId"};
        //  new CreateSql("users").setColumnName(userColumnNames).craeteSqlFile(SqlCRUD.INSERT);
       // new CreateSql("dept").setColumnName(userColumnNames).craeteSqlFile(SqlCRUD.INSERT);
        //new CreateSql("roles").setColumnName(roleColumnNames).craeteSqlFile(SqlCRUD.INSERT);
        new CreateSql("users_roles").setColumnName(user_roleColumnNames).craeteSqlFile(SqlCRUD.INSERT);
    }

}
