package com.yao.core.generator.mybatisplus.config;

import com.baomidou.mybatisplus.generator.config.PackageConfig;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;


/**
 * mybatisplus包配置详细信息请看 PackageConfig
 */

@AllArgsConstructor
@NoArgsConstructor
public class MybatisPlusPackageConfig {

    /**
     * 父包名。如果为空，将下面子包名必须写全部， 否则就只需写子包名
     */
    @Getter
    private String parent = "com.yao";

    /**
     * 父包模块名。
     */
    @Getter
    private String moduleName = null;

    /**
     * Entity包名
     */
    @Getter
    private String entity = "entity";

    /**
     * Service包名
     */
    @Getter
    private String service = "service";

    /**
     * Service Impl包名
     */
    @Getter
    private String serviceImpl = "service.impl";
    /**
     * Mapper包名
     */
    @Getter
    private String mapper = "dao";

    /**
     * Mapper XML包名
     */
    @Getter
    private String xml = "dao.mappers.xml";

    /**
     * Controller包名
     */
    @Getter
    private String controller = "contoller";


    public MybatisPlusPackageConfig setParent(String parent) {
        this.parent = parent;
        return this;
    }

    public MybatisPlusPackageConfig setModuleName(String moduleName) {
        this.moduleName = moduleName;
        return this;
    }

    public MybatisPlusPackageConfig setEntity(String entity) {
        this.entity = entity;
        return this;
    }

    public MybatisPlusPackageConfig setService(String service) {
        this.service = service;
        return this;
    }

    public MybatisPlusPackageConfig setServiceImpl(String serviceImpl) {
        this.serviceImpl = serviceImpl;
        return this;
    }

    public MybatisPlusPackageConfig setMapper(String mapper) {
        this.mapper = mapper;
        return this;
    }

    public MybatisPlusPackageConfig setXml(String xml) {
        this.xml = xml;
        return this;
    }

    public MybatisPlusPackageConfig setController(String controller) {
        this.controller = controller;
        return this;
    }

    public static PackageConfig createPackageConfig(MybatisPlusPackageConfig mybatisPlusPackageConfig){
        return new PackageConfig().setController(mybatisPlusPackageConfig.getController())
                .setEntity(mybatisPlusPackageConfig.getEntity())
                .setParent(mybatisPlusPackageConfig.getParent())
                .setMapper(mybatisPlusPackageConfig.getMapper())
                .setModuleName(mybatisPlusPackageConfig.getModuleName())
                .setService(mybatisPlusPackageConfig.getService())
                .setServiceImpl(mybatisPlusPackageConfig.getServiceImpl())
                .setXml(mybatisPlusPackageConfig.getXml());

    }

    public static PackageConfig createDefaultPackageConfig(){
        return createPackageConfig(new MybatisPlusPackageConfig());
    }
}
