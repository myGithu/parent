package com.yao.core.generator.mybatisplus.config;

import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by ruijie on 2018/1/30.
 * <p>
 * mybatsisPlus全局配置
 * 详细参数请参考 GlobalConfig
 */
@AllArgsConstructor
@NoArgsConstructor
public class MybatisPlusGlobalConfig {

    @Getter
    private String outputDir = "D:\\ideaSpace\\me";
    @Getter
    private Boolean fileOverride = true;//是否覆盖

    @Getter
    private Boolean activeRecord = true;

    @Getter
    private Boolean enableCache = false;// XML 二级缓存

    @Getter
    private Boolean baseResultMap = true;// XML ResultMap

    @Getter
    private Boolean baseColumnList = true;// XML columList

    @Getter
    private String author = "yao";

    public MybatisPlusGlobalConfig setOutputDir(String outputDir) {
        this.outputDir = outputDir;
        return this;
    }

    public MybatisPlusGlobalConfig setFileOverride(Boolean fileOverride) {
        this.fileOverride = fileOverride;
        return this;
    }

    public MybatisPlusGlobalConfig setActiveRecord(Boolean activeRecord) {
        this.activeRecord = activeRecord;
        return this;
    }

    public MybatisPlusGlobalConfig setEnableCache(Boolean enableCache) {
        this.enableCache = enableCache;
        return this;
    }

    public MybatisPlusGlobalConfig setBaseResultMap(Boolean baseResultMap) {
        this.baseResultMap = baseResultMap;
        return this;
    }

    public MybatisPlusGlobalConfig setBaseColumnList(Boolean baseColumnList) {
        this.baseColumnList = baseColumnList;
        return this;
    }

    public MybatisPlusGlobalConfig setAuthor(String author) {
        this.author = author;
        return this;
    }

    public static GlobalConfig createGlobalConfig(MybatisPlusGlobalConfig mybatisPlusGlobalConfig) {
        return new GlobalConfig().setOutputDir(mybatisPlusGlobalConfig.getOutputDir()).setFileOverride(mybatisPlusGlobalConfig.getFileOverride())//是否覆盖
                .setActiveRecord(mybatisPlusGlobalConfig.getActiveRecord())
                .setEnableCache(mybatisPlusGlobalConfig.getEnableCache())// XML 二级缓存
                .setBaseResultMap(mybatisPlusGlobalConfig.getBaseResultMap())// XML ResultMap
                .setBaseColumnList(mybatisPlusGlobalConfig.getBaseColumnList())// XML columList
                .setAuthor(mybatisPlusGlobalConfig.getAuthor());
    }


    public static GlobalConfig createDefaultGlobalConfig() {
        return createGlobalConfig(new MybatisPlusGlobalConfig());
    }

}
