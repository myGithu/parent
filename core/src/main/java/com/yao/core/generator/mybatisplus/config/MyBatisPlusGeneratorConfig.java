package com.yao.core.generator.mybatisplus.config;

import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ruijie on 2018/1/30.
 */

@AllArgsConstructor
@NoArgsConstructor
public class MyBatisPlusGeneratorConfig {

    @Getter
    private GlobalConfig globalConfig = MybatisPlusGlobalConfig.createDefaultGlobalConfig();

    @Getter
    private DataSourceConfig dataSourceConfig = MybatisPlusDataSourceConfig.createDefaultDataSourceConfig();

    @Getter
    private StrategyConfig strategyConfig = MybatisPlusStrategyConfig.createDefaultStrategyConfig();

    @Getter
    private PackageConfig packageConfig = MybatisPlusPackageConfig.createDefaultPackageConfig();

    @Getter
    private InjectionConfig injectionConfig = DEFAULT_INJECTIONCONFIG;

    public MyBatisPlusGeneratorConfig setGlobalConfig(GlobalConfig globalConfig) {
        this.globalConfig = globalConfig;
        return this;
    }

    public MyBatisPlusGeneratorConfig setDataSourceConfig(DataSourceConfig dataSourceConfig) {
        this.dataSourceConfig = dataSourceConfig;
        return this;
    }

    public MyBatisPlusGeneratorConfig setStrategyConfig(StrategyConfig strategyConfig) {
        this.strategyConfig = strategyConfig;
        return this;
    }

    public MyBatisPlusGeneratorConfig setPackageConfig(PackageConfig packageConfig) {
        this.packageConfig = packageConfig;
        return this;
    }

    public MyBatisPlusGeneratorConfig setInjectionConfig(InjectionConfig injectionConfig) {
        this.injectionConfig = injectionConfig;
        return this;
    }

    public static MyBatisPlusGeneratorConfig createDefaultMyBatisPlusGeneratorConfig(){
        return new MyBatisPlusGeneratorConfig();
    }

    private static final InjectionConfig DEFAULT_INJECTIONCONFIG = new InjectionConfig() {
        @Override
        public void initMap() {
            Map<String, Object> map = new HashMap<>();
            map.put("abc",this.getConfig().getGlobalConfig().getAuthor() + "-mp");
            this.setMap(map);
        }
    };

}
