package com.yao.core.generator.mybatisplus.config;

import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by ruijie on 2018/1/30.
 * mybatisplus 数据库配置详细参数请参考 DataSourceConfig
 */

@AllArgsConstructor
@NoArgsConstructor
public class MybatisPlusDataSourceConfig {
    @Getter
    private DbType dbType = DbType.MYSQL;
    @Getter
    private String driverName = "com.mysql.jdbc.Driver";

    @Getter
    private String username = "root";

    @Getter
    private String password = "root";

    @Getter
    private String url = "jdbc:mysql://127.0.0.1:3306/springboots?characterEncoding=utf8";

    public MybatisPlusDataSourceConfig setDbType(DbType dbType) {
        this.dbType = dbType;
        return this;
    }

    public MybatisPlusDataSourceConfig setDriverName(String driverName) {
        this.driverName = driverName;
        return this;
    }

    public MybatisPlusDataSourceConfig setUsername(String username) {
        this.username = username;
        return this;
    }

    public MybatisPlusDataSourceConfig setPassword(String password) {
        this.password = password;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public static DataSourceConfig createDataSourceConfig(MybatisPlusDataSourceConfig mybatisPlusDataSourceConfig) {
        return new DataSourceConfig()
                .setDbType(mybatisPlusDataSourceConfig.getDbType())
                .setDriverName(mybatisPlusDataSourceConfig.getDriverName())
                .setUsername(mybatisPlusDataSourceConfig.getUsername())
                .setPassword(mybatisPlusDataSourceConfig.getPassword())
                .setUrl(mybatisPlusDataSourceConfig.getUrl());
    }

    public static DataSourceConfig createDefaultDataSourceConfig() {
        return createDataSourceConfig(new MybatisPlusDataSourceConfig());
    }
}
