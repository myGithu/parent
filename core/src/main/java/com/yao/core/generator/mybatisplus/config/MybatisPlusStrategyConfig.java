package com.yao.core.generator.mybatisplus.config;

import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * mybatisplus策略配置 详细 参数请看StrategyConfig
 */

@AllArgsConstructor
@NoArgsConstructor
public class MybatisPlusStrategyConfig {

    /**
     *  表前缀
     */
    @Getter
    private String[] tablePrefix;

    /**
     * 驼峰命名法
     */
    @Getter
    private NamingStrategy naming = NamingStrategy.underline_to_camel;

    /**
     *
     * 需要生成的表名
     *
     */
    @Getter
    private String[] include = null;

    public MybatisPlusStrategyConfig setTablePrefix(String[] tablePrefix) {
        this.tablePrefix = tablePrefix;
        return this;
    }

    public MybatisPlusStrategyConfig setNaming(NamingStrategy naming) {
        this.naming = naming;
        return this;
    }

    public MybatisPlusStrategyConfig setInclude(String[] include) {
        this.include = include;
        return this;
    }

    public static StrategyConfig createStrategyConfig(MybatisPlusStrategyConfig mybatisPlusStrategyConfig){
        return new StrategyConfig().setTablePrefix(mybatisPlusStrategyConfig.getTablePrefix())
                .setNaming(mybatisPlusStrategyConfig.getNaming())
                .setInclude(mybatisPlusStrategyConfig.getInclude());
    }

    public static StrategyConfig createDefaultStrategyConfig(){
        return  createStrategyConfig(new MybatisPlusStrategyConfig());
    }

}
