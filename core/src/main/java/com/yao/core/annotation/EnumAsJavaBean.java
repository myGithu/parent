package com.yao.core.annotation;

import java.lang.annotation.*;

/**
 * Created by ruijie on 2018/1/27.
 * EnumAsJavaBean标记注解用于fastjson配置
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface EnumAsJavaBean {

}
