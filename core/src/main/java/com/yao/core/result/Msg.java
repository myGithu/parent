package com.yao.core.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ruijie on 2018/2/15.
 * 返回不同角色的用户信息
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Msg {
        private String title;

        private String content;

        private String etraInfo;
}
