package com.yao.core.result;


import com.yao.core.annotation.EnumAsJavaBean;
import com.yao.core.util.DateTimeUtil;
import lombok.Getter;

/**
 * Created by ruijie on 2017/12/21.
 */
@EnumAsJavaBean
public enum Result {
    SUCCESS("0000", "SUCCESS", "操作成功！"),
    ERROR("1111", "ERROR", "操作失败"),

    CODE_SUCCESS("0001","CODE_SUCCESS","验证码正确"),
    CODE_ERROR("1110","CODE_ERROR","验证码错误");

    @Getter
    private final String resultCode;

    @Getter
    private final String resultMsg;

    @Getter
    private final String remarksMsg;

    @Getter
    private final String createDateTime;

    Result(String resultCode, String resultMsg, String remarksMsg) {
        this.resultCode = resultCode;
        this.resultMsg = resultMsg;
        this.remarksMsg = remarksMsg;
        this.createDateTime = DateTimeUtil.getStringNowDateTime();
    }

}
