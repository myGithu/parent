/**
 * Created by ruijie on 2017/6/1.
 */
const SystemUtil = {
    /*根据地址栏url中的key获取value*/
    getUrlParam: function getUrlParam(paramKey) {
        var reg = new RegExp("(^|\\?|&)" + paramKey + "=([^&]*)(\\s|&|$)", "i");
        if (reg.test(location.href))
            return decodeURI(RegExp.$2.replace(/\+/g, " "));
        return "";
    },
    /**
     * 判断字符串或者数组是否为空
     * @param obj
     * @returns {boolean}
     */
    isEmpty: function isEmpty(obj) {
        if (obj instanceof Array) {
            if (obj.length <= 0 || null == obj) {
                return true;
            }
        }
        if (obj instanceof String || typeof obj == 'string') {
            if ('' == obj || null == obj || undefined == obj) {
                return true;
            }
        }
        return false;
    }
};

const StringUtil = {};

const CheckUtil = {
    /**
     * 验证邮件是否合法
     * @param emailStr 需要验证的邮件
     * @returns {Boolean} true为合法 false为不合法
     */
    checkEmail: function checkEmail(emailStr) {
        var objReg = new RegExp("[A-Za-z0-9-_]+@[A-Za-z0-9-_]+[\.][A-Za-z0-9-_]");
        var IsRightFmt = objReg.test(emailStr);
        var objRegErrChar = new RegExp("[^a-z0-9-._@]", "ig");
        var IsRightChar = (emailStr.search(objRegErrChar) == -1);
        var IsRightLength = emailStr.length <= 60;
        var IsRightPos = (emailStr.indexOf("@", 0) != 0 && emailStr.indexOf(".", 0) != 0 && emailStr.lastIndexOf("@") + 1 != emailStr.length && emailStr.lastIndexOf(".") + 1 != emailStr.length);
        var IsNoDupChar = (emailStr.indexOf("@", 0) == emailStr.lastIndexOf("@"));
        if (IsRightFmt && IsRightChar && IsRightLength && IsRightPos && IsNoDupChar) {
            return true;
        } else {
            return false;
        }
    },

    /**
     * 验证身份证号码是否合法
     * @param idNo 身份证号码
     * @returns {Boolean} true为合法 false为不合法
     */
    checkIdNo: function checkIdNo(idNo) {
        var regex = '';
        if (idNo.length == 0) {
            return false;
        }
        //var errors = new Array("","身份证号码位数不对!","身份证号码出生日期超出范围或含有非法字符!","身份证号码校验错误!","身份证地区非法!");
        var area = {
            11: "北京",
            12: "天津",
            13: "河北",
            14: "山西",
            15: "内蒙古",
            21: "辽宁",
            22: "吉林",
            23: "黑龙江",
            31: "上海",
            32: "江苏",
            33: "浙江",
            34: "安徽",
            35: "福建",
            36: "江西",
            37: "山东",
            41: "河南",
            42: "湖北",
            43: "湖南",
            44: "广东",
            45: "广西",
            46: "海南",
            50: "重庆",
            51: "四川",
            52: "贵州",
            53: "云南",
            54: "西藏",
            61: "陕西",
            62: "甘肃",
            63: "青海",
            64: "宁夏",
            65: "新疆",
            71: "台湾",
            81: "香港",
            82: "澳门",
            91: "国外"
        };
        var Y, JYM;
        var S, M;
        if (!/^\d{17}(\d|x)$/i.test(idNo)) {
            return false;
        }
        var idcard_array = new Array();
        idcard_array = idNo.split("");
        if (area[parseInt(idNo.substr(0, 2))] == null) {
            return false;
        }
        switch (idNo.length) {
            case 15:
                if ((parseInt(idNo.substr(6, 2)) + 1900) % 4 == 0 || ((parseInt(idNo.substr(6, 2)) + 1900) % 100 == 0 && (parseInt(idNo.substr(6, 2)) + 1900) % 4 == 0 )) {
                    regex = /^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}$/;//测试出生日期的合法性
                } else {
                    regex = /^[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}$/;//测试出生日期的合法性
                }
                if (regex.test(idNo)) {
                    return true;
                } else {
                    return false;
                }
                break;
            case 18:
                if (parseInt(idNo.substr(6, 4)) % 4 == 0 || (parseInt(idNo.substr(6, 4)) % 100 == 0 && parseInt(idNo.substr(6, 4)) % 4 == 0 )) {
                    regex = /^[1-9][0-9]{5}(19|20)[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}[0-9Xx]$/;//闰年出生日期的合法性正则表达式
                } else {
                    regex = /^[1-9][0-9]{5}(19|20)[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}[0-9Xx]$/;//平年出生日期的合法性正则表达式
                }
                if (regex.test(idNo)) {
                    S = (parseInt(idcard_array[0]) + parseInt(idcard_array[10])) * 7 + (parseInt(idcard_array[1]) + parseInt(idcard_array[11])) * 9 + (parseInt(idcard_array[2]) + parseInt(idcard_array[12])) * 10 + (parseInt(idcard_array[3]) + parseInt(idcard_array[13])) * 5 + (parseInt(idcard_array[4]) + parseInt(idcard_array[14])) * 8 + (parseInt(idcard_array[5]) + parseInt(idcard_array[15])) * 4 + (parseInt(idcard_array[6]) + parseInt(idcard_array[16])) * 2 + parseInt(idcard_array[7]) * 1 + parseInt(idcard_array[8]) * 6 + parseInt(idcard_array[9]) * 3;
                    Y = S % 11;
                    M = "F";
                    JYM = "10X98765432";
                    M = JYM.substr(Y, 1);
                    if (M == idcard_array[17]) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
                break;
            default:
                return false;
                break;
        }
    },

    /**
     * 验证电话是否合法
     * @param telePhonestr 电话号码
     * @returns true为合法 false为不合法
     */
    checkTelePhone: function checkTelePhone(telePhonestr) {
        //电话号码
        var pattern = /^((\(\d{3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}$/;
        return pattern.test(telePhonestr);
    },
    /**
     * 验证手机号码是否合法
     * @param mobilephoneStr 手机号码
     * @returns true为合法 false为不合法
     */
    checkMobilephone: function checkMobilephone(mobilephoneStr) {
        //手机号码
        var pattern = /^1[3,4,5,7,8]\d{9}$/;
        return pattern.test(mobilephoneStr);
    },
    /**
     * 验证QQ是否合法
     * @param qqStr QQ号码
     * @returns true为合法 false为不合法
     */
    checkQQ: function checkQQ(qqStr) {
        //腾讯QQ
        var pattern = /^[1-9][0-9]{4,}$/;
        return pattern.test(qqStr);
    },

    /**
     * 验证密钥是否合法
     * @param key 密钥
     * @returns true为合法 false为不合法
     */
    checkKey: function checkKey(key) {
        var pattern = /^[0-9]{1,3}$/;
        return pattern.test(key);
    },

    /**
     * 验证用户名是否合法
     * @param userName 用户名
     * @returns true为合法 false为不合法
     */
    checkUserName: function checkUserName(userName) {
        //用户名
        var pattern = /^[a-zA-Z0-9_]{1,20}$/;
        return pattern.test(userName);
    },
    /**
     * 验证个人网站是否合法
     * @param str_url URL
     * @returns {Boolean} true为合法 false为不合法
     */
    checkURL: function checkURL(str_url) {
        var strRegex = "^((https|http|ftp|rtsp|mms)?://)"
            + "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?" //ftp的user@
            + "(([0-9]{1,3}.){3}[0-9]{1,3}" // IP形式的URL- 199.194.52.184
            + "|" // 允许IP和DOMAIN（域名）
            + "([0-9a-z_!~*'()-]+.)*" // 域名- www.
            + "([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]." // 二级域名
            + "[a-z]{2,6})" // first level domain- .com or .museum
            + "(:[0-9]{1,4})?" // 端口- :80
            + "((/?)|" // a slash isn't required if there is no file name
            + "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$";
        var re = new RegExp(strRegex);
        if (re.test(str_url)) {
            return (true);
        } else {
            return (false);
        }
    },
    /**
     * 验证邮编是否合法
     * @param postCodeStr 邮编号码
     * @returns true为合法 false为不合法
     */
    checkpostCode: function checkpostCode(postCodeStr) {
        //邮编
        var pattern = /^[1-9]\d{5}$/;
        return pattern.test(postCodeStr);
    },
    /**
     * 检测图片类型是否支持
     * @param picType 图片类型 如：".jpg"
     * @returns {Boolean} true为合法 false为不合法
     */
    checkPicType: function checkPicType(picType) {
        var picTypeArray = new Array(".jpg", ".jpeg", ".png", ".bmp", ".gif");
        for (var i = 0; i < picTypeArray.length; i++) {
            if (picTypeArray[i] == picType) {
                return true;
            }
        }
        return false;
    },


    /**
     * 判断价格是否合法
     * @param str
     * @returns
     */
    checkPrice: function checkPrice(str) {
        var patrn = /^\d+(\.[0-9]{0,2})?$/;
        var result = patrn.exec(str);
        if (result == null) {
            return false;
        }
        return result;
    },
    /**
     * 判断是否为整形
     * @param str
     * @returns
     */
    checkInteger: function checkInteger(str) {
        var patrn = /^\d{1,11}$/;
        return patrn.exec(str);
    },
    /**
     * 判断日期格式是否正确：yyyy-MM-dd HH:mm (没有秒)
     * @param str
     * @returns
     */
    checkDate: function checkDate(str) {
        var patrn = /^((\d{2}(([02468][048])|([13579][26]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|([1-2][0-9])))))|(\d{2}(([02468][1235679])|([13579][01345789]))[\-\/\s]?((((0?[13578])|(1[02]))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\-\/\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\-\/\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\s((([0-1][0-9])|(2?[0-3]))\:([0-5]?[0-9])))?$/;
        return patrn.exec(str);
    },
    /**
     * 判断日期格式是否正确：yyyy-MM-dd
     * @param str
     * @returns {Boolean}
     */
    checkDateYYYYMMDD: function checkDateYYYYMMDD(str) {
        if (!/^19\d\d-[0-1]\d-[0-3]\d+$/.test(str) && !/^20\d\d-[0-1]\d-[0-3]\d+$/.test(str)) {
            return false;
        } else {
            return true;
        }
    }
};

const DateUtil = {
    patterns: {
        PATTERN_ERA: 'G', //Era 标志符 Era strings. For example: "AD" and "BC"
        PATTERN_YEAR: 'y', //年
        PATTERN_MONTH: 'M', //月份
        PATTERN_DAY_OF_MONTH: 'd', //月份的天数
        PATTERN_HOUR_OF_DAY1: 'k', //一天中的小时数（1-24）
        PATTERN_HOUR_OF_DAY0: 'H', //24小时制，一天中的小时数（0-23）
        PATTERN_MINUTE: 'm', //小时中的分钟数
        PATTERN_SECOND: 's', //秒
        PATTERN_MILLISECOND: 'S', //毫秒
        PATTERN_DAY_OF_WEEK: 'E', //一周中对应的星期，如星期一，周一
        PATTERN_DAY_OF_YEAR: 'D', //一年中的第几天
        PATTERN_DAY_OF_WEEK_IN_MONTH: 'F', //一月中的第几个星期(会把这个月总共过的天数除以7,不够准确，推荐用W)
        PATTERN_WEEK_OF_YEAR: 'w', //一年中的第几个星期
        PATTERN_WEEK_OF_MONTH: 'W', //一月中的第几星期(会根据实际情况来算)
        PATTERN_AM_PM: 'a', //上下午标识
        PATTERN_HOUR1: 'h', //12小时制 ，am/pm 中的小时数（1-12）
        PATTERN_HOUR0: 'K', //和h类型
        PATTERN_ZONE_NAME: 'z', //时区名
        PATTERN_ZONE_VALUE: 'Z', //时区值
        PATTERN_WEEK_YEAR: 'Y', //和y类型
        PATTERN_ISO_DAY_OF_WEEK: 'u',
        PATTERN_ISO_ZONE: 'X'
    },
    week: {
        'ch': {
            "0": "\u65e5",
            "1": "\u4e00",
            "2": "\u4e8c",
            "3": "\u4e09",
            "4": "\u56db",
            "5": "\u4e94",
            "6": "\u516d"
        },
        'en': {
            "0": "Sunday",
            "1": "Monday",
            "2": "Tuesday",
            "3": "Wednesday",
            "4": "Thursday",
            "5": "Friday",
            "6": "Saturday"
        }
    },
    //获取当前时间
    getCurrentTime: function () {
        var today = new Date();
        var year = today.getFullYear();
        var month = today.getMonth() + 1;
        var day = today.getDate();
        var hours = today.getHours();
        var minutes = today.getMinutes();
        var seconds = today.getSeconds();
        var timeString = year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
        return timeString;
    },
    /*
     * 比较时间大小
     * time1>time2 return 1
     * time1<time2 return -1
     * time1==time2 return 0
     */
    compareTime: function (time1, time2) {
        if (Date.parse(time1.replace(/-/g, "/")) > Date.parse(time2.replace(/-/g, "/"))) {
            return 1;
        } else if (Date.parse(time1.replace(/-/g, "/")) < Date.parse(time2.replace(/-/g, "/"))) {
            return -1;
        } else if (Date.parse(time1.replace(/-/g, "/")) == Date.parse(time2.replace(/-/g, "/"))) {
            return 0;
        }
    },
    //是否闰年
    isLeapYear: function (year) {
        return ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0);
    },
    //获取某个月的天数，从0开始
    getDaysOfMonth: function (year, month) {
        return [31, (this.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
    },
    getDaysOfMonth2: function (year, month) {
        // 将天置为0，会获取其上个月的最后一天
        month = parseInt(month) + 1;
        var date = new Date(year, month, 0);
        return date.getDate();
    },
    /*距离现在几天的日期：负数表示今天之前的日期，0表示今天，整数表示未来的日期
     * 如-1表示昨天的日期，0表示今天，2表示后天
     */
    fromToday: function (days) {
        var today = new Date();
        today.setDate(today.getDate() + days);
        var date = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
        return date;
    },
    /**
     * 日期时间格式化
     * @param {Object} dateTime 需要格式化的日期时间
     * @param {String} pattern 格式化的模式，如yyyy-MM-dd hh(HH):mm:ss.S a k K E D F w W z Z
     */
    formt: function (dateTime, pattern) {
        const DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ss";
        var dateTime = dateTime || DateUtil.getCurrentTime();
        var pattern = pattern || DEFAULT_FORMAT;
        var date = new Date(dateTime);
        return pattern.replace(/([a-z])\1*/ig, function (matchStr, group1) {
            var replacement = "";
            switch (group1) {
                case DateUtil.patterns.PATTERN_ERA: //G
                    break;
                case DateUtil.patterns.PATTERN_WEEK_YEAR: //Y
                case DateUtil.patterns.PATTERN_YEAR: //y
                    replacement = date.getFullYear();
                    break;
                case DateUtil.patterns.PATTERN_MONTH: //M
                    var month = date.getMonth() + 1;
                    replacement = (month < 10 && matchStr.length >= 2) ? "0" + month : month;
                    break;
                case DateUtil.patterns.PATTERN_DAY_OF_MONTH: //d
                    var days = date.getDate();
                    replacement = (days < 10 && matchStr.length >= 2) ? "0" + days : days;
                    break;
                case DateUtil.patterns.PATTERN_HOUR_OF_DAY1: //k(1~24)
                    var hours24 = date.getHours();
                    replacement = hours24;
                    break;
                case DateUtil.patterns.PATTERN_HOUR_OF_DAY0: //H(0~23)
                    var hours24 = date.getHours();
                    replacement = (hours24 < 10 && matchStr.length >= 2) ? "0" + hours24 : hours24;
                    break;
                case DateUtil.patterns.PATTERN_MINUTE: //m
                    var minutes = date.getMinutes();
                    replacement = (minutes < 10 && matchStr.length >= 2) ? "0" + minutes : minutes;
                    break;
                case DateUtil.patterns.PATTERN_SECOND: //s
                    var seconds = date.getSeconds();
                    replacement = (seconds < 10 && matchStr.length >= 2) ? "0" + seconds : seconds;
                    break;
                case DateUtil.patterns.PATTERN_MILLISECOND: //S
                    var milliSeconds = date.getMilliseconds();
                    replacement = milliSeconds;
                    break;
                case DateUtil.patterns.PATTERN_DAY_OF_WEEK: //E
                    var day = date.getDay();
                    replacement = Bee.DateUtils.week['ch'][day];
                    break;
                case DateUtil.patterns.PATTERN_DAY_OF_YEAR: //D
                    replacement = DateUtil.dayOfTheYear(date);
                    break;
                case DateUtil.patterns.PATTERN_DAY_OF_WEEK_IN_MONTH: //F
                    var days = date.getDate();
                    replacement = Math.floor(days / 7);
                    break;
                case DateUtil.patterns.PATTERN_WEEK_OF_YEAR: //w
                    var days = Bee.DateUtils.dayOfTheYear(date);
                    replacement = Math.ceil(days / 7);
                    break;
                case DateUtil.patterns.PATTERN_WEEK_OF_MONTH: //W
                    var days = date.getDate();
                    replacement = Math.ceil(days / 7);
                    break;
                case DateUtil.patterns.PATTERN_AM_PM: //a
                    var hours24 = date.getHours();
                    replacement = hours24 < 12 ? "\u4e0a\u5348" : "\u4e0b\u5348";
                    break;
                case DateUtil.patterns.PATTERN_HOUR1: //h(1~12)
                    var hours12 = date.getHours() % 12 || 12; //0转为12
                    replacement = (hours12 < 10 && matchStr.length >= 2) ? "0" + hours12 : hours12;
                    break;
                case DateUtil.patterns.PATTERN_HOUR0: //K(0~11)
                    var hours12 = date.getHours() % 12;
                    replacement = hours12;
                    break;
                case DateUtil.patterns.PATTERN_ZONE_NAME: //z
                    replacement = DateUtil.getZoneNameValue(date)['name'];
                    break;
                case DateUtil.patterns.PATTERN_ZONE_VALUE: //Z
                    replacement = DateUtil.getZoneNameValue(date)['value'];
                    break;
                case DateUtil.patterns.PATTERN_ISO_DAY_OF_WEEK: //u
                    break;
                case DateUtil.patterns.PATTERN_ISO_ZONE: //X
                    break;
                default:
                    break;
            }
            return replacement;
        });
    },
    /**
     * 计算一个日期是当年的第几天
     * @param {Object} date
     */
    dayOfTheYear: function (date) {
        var obj = new Date(date);
        var year = obj.getFullYear();
        var month = obj.getMonth(); //从0开始
        var days = obj.getDate();
        var daysArr = [31, (this.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        for (var i = 0; i < month; i++) {
            days += daysArr[i];
        }
        return days;
    },
    //获得时区名和值
    getZoneNameValue: function (dateObj) {
        var date = new Date(dateObj);
        date = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
        var arr = date.toString().match(/([A-Z]+)([-+]\d+:?\d+)/);
        var obj = {
            'name': arr[1],
            'value': arr[2]
        };
        return obj;
    }
};
