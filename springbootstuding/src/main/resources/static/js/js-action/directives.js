/**
 * Created by ruijie on 2018/1/25.
 * 自定义指令
 */

actionApp.directive('datePicker',function(){
    return {
        restrict : "AC",
        link : function (scope,elem,attrs) {
            elem.datepicker();
        }
    };
});
