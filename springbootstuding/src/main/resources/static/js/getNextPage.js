/**
 * Created by ruijie on 2017/9/2.
 * h5下拉到底部是加载下一页数据
 */
//页面拉到底时自动加载更多
$(window).scroll(function(event){
    var wScrollY = window.scrollY; // 当前滚动条位置
    var wInnerH = window.innerHeight; // 设备窗口的高度（不会变）
    var bScrollH = document.body.scrollHeight; // 滚动条总高度
    if (wScrollY + wInnerH >= bScrollH) {
        getNext();
    }
});
/**
 * 下一页
 */
function getNext() {
    // 获取当前所在的页数
    var currentPage = pageCount;
    // 总页数
    var sumPage = 0;
    if (sumcount % count == 0) {
        sumPage = sumcount / count;
    } else {
        sumPage = parseInt((sumcount / count)) + 1;
    }
    if (currentPage == sumPage) {
        $('#getNext').html("我是有底线的");
        return ;
    } else {
        var downnum = parseInt(currentPage) + 1;
        pageCount = downnum;
    }
}