/**
 * Created by ruijie on 2018/1/17.
 */

/*服务器地址*/
const SERVER_URL = 'http://127.0.0.1:9191/springbootstuding';

function addCsrfRequestHeader(){
    var token = $("meta[name='_csrf']").attr('content');
    var header = $("meta[name='_csrf_header']").attr('content');
    $(document).ajaxSend(function(e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });
}

