package com.yao.utils;

import com.yao.entity.Schedule;
import com.yao.quartz.SysQuartzKey;
import com.yao.quartz.job.ScheduledJob;
import lombok.extern.log4j.Log4j2;
import org.quartz.*;

/**
 * 定时任务辅助类
 * Created by ruijie on 2018/2/19.
 */
@Log4j2
public class ScheduleUtil {


    /**
     * 获取触发器的key
     *
     * @param jobName  业务名称
     * @param jobGroup 业务组
     * @return TriggerKey
     */
    public static TriggerKey getTriggerKey(String jobName, String jobGroup) {
        return TriggerKey.triggerKey(jobName, jobGroup);
    }

    /**
     * @param scheduler 调度器
     * @param schedule  任务对象
     * @throws SchedulerException
     */
    public static void createScheduleJob(Scheduler scheduler, Schedule schedule) throws SchedulerException {
        createScheduleJob(scheduler, schedule.getJobName(), schedule.getJobGroup(), schedule.getCronExpression(), schedule);
    }


    /**
     * 创建定时任务
     *
     * @param scheduler      调度器
     * @param jobName        业务名称
     * @param jobGroup       业务组
     * @param cronExpression cron表达式
     * @param o              参数对象
     * @throws SchedulerException
     */
    public static void createScheduleJob(Scheduler scheduler, String jobName,
                                         String jobGroup, String cronExpression, Object o) throws SchedulerException {
        JobDetail jobDetail = JobBuilder.newJob(ScheduledJob.class)
                .withIdentity(jobName, jobGroup)
                .build();
        //放入参数，运行时的方法可以获取
        jobDetail.getJobDataMap().put(SysQuartzKey.JOB_PARAM_KEY, o);

        // 表达式调度构建器（可判断创建SimpleScheduleBuilder）
        CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(cronExpression);

        // 按新的cronExpression表达式构建一个新的trigger
        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity(jobName, jobGroup)
                .withSchedule(cronScheduleBuilder)
                .build();
        try {
            scheduler.scheduleJob(jobDetail, trigger);
        } catch (SchedulerException e) {
            log.error("创建定时任务失败！", e);
            throw new SchedulerException("创建定时任务失败！");
        }

    }

    /**
     * 立即运行一次定时任务
     *
     * @param scheduler 调度器
     * @param jobName   业务名称
     * @param jobGroup  业务组
     * @throws SchedulerException
     */
    public static void startNowJob(Scheduler scheduler, String jobName, String jobGroup) throws SchedulerException {
        try {
            scheduler.triggerJob(getJobKey(jobName, jobGroup));
        } catch (SchedulerException e) {
            log.error("运行一次定时任务失败", e);
            throw new SchedulerException("运行一次定时任务失败");
        }
    }

    /**
     * 暂停定时任务
     * @param scheduler 调度器
     * @param jobName   业务名称
     * @param jobGroup  业务组
     * @throws SchedulerException
     */
    public static void pauseJob(Scheduler scheduler, String jobName, String jobGroup) throws SchedulerException {
        try {
            scheduler.pauseJob(getJobKey(jobName, jobGroup));
        } catch (SchedulerException e) {
            log.error("任务暂停失败", e);
            throw new SchedulerException("任务暂停失败");
        }
    }

    /**
     * 恢复定时任务
     * @param scheduler 调度器
     * @param jobName   业务名称
     * @param jobGroup  业务组
     * @throws SchedulerException
     */
    public static void resumeJob(Scheduler scheduler, String jobName, String jobGroup) throws SchedulerException {
        try {
            scheduler.resumeJob(getJobKey(jobName, jobGroup));
        } catch (SchedulerException e) {
            log.error("任务恢复失败", e);
            throw new SchedulerException("任务恢复失败");
        }
    }

    /**
     * 更新定时任务
     * @param scheduler 调度器
     * @param schedule  任务对象
     */
    public static void updateScheduleJob(Scheduler scheduler, Schedule schedule) throws SchedulerException {
        updateScheduleJob(scheduler, schedule.getJobName(), schedule.getJobGroup(), schedule.getCronExpression());
    }

    /**
     * 更新定时任务
     * @param scheduler 调度器
     * @param jobName   业务名称
     * @param jobGroup  业务组
     * @param cronExpression cron表达式
     * @throws SchedulerException
     */
    public static void updateScheduleJob(Scheduler scheduler, String jobName,
                                         String jobGroup, String cronExpression) throws SchedulerException {

        TriggerKey triggerKey = getTriggerKey(jobName, jobGroup);

        // 表达式调度构建器（可判断创建SimpleScheduleBuilder）
        CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(cronExpression);

        // 按新的cronExpression表达式构建一个新的trigger
        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity(triggerKey)
                .withSchedule(cronScheduleBuilder)
                .build();
        try {
            //按新的trigger重新设置job执行
            scheduler.rescheduleJob(triggerKey, trigger);
        } catch (SchedulerException e) {
            log.error("任务更新失败", e);
            throw new SchedulerException("任务更新失败");
        }
    }

    /**
     * 删除定时任务
     * @param scheduler 调度器
     * @param jobName   业务名称
     * @param jobGroup  业务组
     * @throws SchedulerException
     */
    public static void deleteScheduleJob(Scheduler scheduler, String jobName, String jobGroup) throws SchedulerException {
        try {
            scheduler.deleteJob(getJobKey(jobName, jobGroup));
        } catch (SchedulerException e) {
            log.error("任务删除失败", e);
            throw new SchedulerException("任务删除失败");
        }
    }

    /**
     *  获取业务的Key值
     *
     * @param jobName   业务名称
     * @param jobGroup  业务组
     * @return
     */
    public static JobKey getJobKey(String jobName, String jobGroup) {
        return JobKey.jobKey(jobName, jobGroup);
    }

}
