package com.yao.modular.system;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import com.yao.core.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Created by ruijie on 2018/1/27.
 * 验证码生成
 */
@Controller
@RequestMapping("/kaptcha")
public class KaptchaController {
    @Autowired
    private Producer producer;

    @RequestMapping("/getVerificationCode")
    public void getVerificationCode(HttpServletRequest request, HttpServletResponse response){
        HttpSession session = request.getSession();
        response.setDateHeader("Expires",0);

        /**
         *
         * 定义response输出类型为image/jpeg类型，使用response输出流输出图片的byte数组
         */

        // Set standard HTTP/1.1 no-cache headers.
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");

        // Set IE extended HTTP/1.1 no-cache headers (use addHeader).
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");

        // Set standard HTTP/1.0 no-cache header.
        response.setHeader("Pragma", "no-cache");

        // return a jpeg
        response.setContentType("image/jpeg");

        //生成验证码内容
        String createText = producer.createText();
        //将内容放到session中
        session.setAttribute(Constants.KAPTCHA_SESSION_KEY,createText);

        BufferedImage createImage =  producer.createImage(createText);
        try (ServletOutputStream out = response.getOutputStream()){
            ImageIO.write(createImage,"jpg",out);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @ResponseBody
    @PostMapping("/checkVerificationCode")
    public Result checkVerificationCode(@RequestParam("checkCode") String checkCode,HttpServletRequest request){
            String codeText = (String) request.getSession().getAttribute(Constants.KAPTCHA_SESSION_KEY);
            if(codeText.equals(checkCode)){
                return Result.CODE_SUCCESS;
            }else{
                return Result.CODE_ERROR;
            }
    }
}
