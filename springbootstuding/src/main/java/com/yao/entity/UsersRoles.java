package com.yao.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yao
 * @since 2018-02-15
 */
@TableName("users_roles")
public class UsersRoles extends Model<UsersRoles> {

    private static final long serialVersionUID = 1L;

	@TableId(value="usersRolesId", type= IdType.AUTO)
	private Integer usersRolesId;
	private String userId;
	private String roleId;


	public Integer getUsersRolesId() {
		return usersRolesId;
	}

	public void setUsersRolesId(Integer usersRolesId) {
		this.usersRolesId = usersRolesId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	@Override
	protected Serializable pkVal() {
		return this.usersRolesId;
	}

	@Override
	public String toString() {
		return "UsersRoles{" +
			"usersRolesId=" + usersRolesId +
			", userId=" + userId +
			", roleId=" + roleId +
			"}";
	}
}
