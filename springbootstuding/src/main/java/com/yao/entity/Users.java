package com.yao.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author yao
 * @since 2018-02-15
 */

public class Users extends Model<Users> {

    private static final long serialVersionUID = 1L;

	@TableId(value="userId", type= IdType.AUTO)
	private Integer userId;
	private String deptNo;
	private String userNo;
	private String userName;
	private String userSex;
	private String userTel;
	private String createTime;
	private String password;
	private List<Roles> roles;

	public Users() {
	}

	public Users(Integer userId, String deptNo, String userNo, String userName, String userSex, String userTel, String createTime, String password, List<Roles> roles) {
		this.userId = userId;
		this.deptNo = deptNo;
		this.userNo = userNo;
		this.userName = userName;
		this.userSex = userSex;
		this.userTel = userTel;
		this.createTime = createTime;
		this.password = password;
		this.roles = roles;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserSex() {
		return userSex;
	}

	public void setUserSex(String userSex) {
		this.userSex = userSex;
	}

	public String getUserTel() {
		return userTel;
	}

	public void setUserTel(String userTel) {
		this.userTel = userTel;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Roles> getRoles() {
		return roles;
	}

	public void setRoles(List<Roles> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "Users{" +
				"userId=" + userId +
				", deptNo='" + deptNo + '\'' +
				", userNo='" + userNo + '\'' +
				", userName='" + userName + '\'' +
				", userSex='" + userSex + '\'' +
				", userTel='" + userTel + '\'' +
				", createTime='" + createTime + '\'' +
				", password='" + password + '\'' +
				", roles=" + roles +
				'}';
	}

	@Override
	protected Serializable pkVal() {
		return this.userId;
	}


}
