package com.yao.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yao
 * @since 2018-02-15
 */
public class Roles extends Model<Roles> {

    private static final long serialVersionUID = 1L;

	@TableId(value="roleId", type= IdType.AUTO)
	private Integer roleId;
	private String roleName;


	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	@Override
	protected Serializable pkVal() {
		return this.roleId;
	}

	@Override
	public String toString() {
		return "Roles{" +
			"roleId=" + roleId +
			", roleName=" + roleName +
			"}";
	}
}
