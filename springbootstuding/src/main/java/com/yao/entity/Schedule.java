package com.yao.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yao
 * @since 2018-02-18
 */
@AllArgsConstructor
@NoArgsConstructor
public class Schedule extends Model<Schedule> {

    private static final long serialVersionUID = 1L;

	@TableId(value="scheduleId", type= IdType.AUTO)
	private Integer scheduleId;
	private String jobName;
	private String jobGroup;
	private String cronExpression;
	private String status;
	private String description;
	private String className;
	private String methodName;

	public Schedule(String jobName, String jobGroup, String cronExpression, String status, String description, String className, String methodName) {
		this.jobName = jobName;
		this.jobGroup = jobGroup;
		this.cronExpression = cronExpression;
		this.status = status;
		this.description = description;
		this.className = className;
		this.methodName = methodName;
	}

	public Integer getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobGroup() {
		return jobGroup;
	}

	public void setJobGroup(String jobGroup) {
		this.jobGroup = jobGroup;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	@Override
	protected Serializable pkVal() {
		return this.scheduleId;
	}

	@Override
	public String toString() {
		return "Schedule{" +
			"scheduleId=" + scheduleId +
			", jobName=" + jobName +
			", jobGroup=" + jobGroup +
			", cronExpression=" + cronExpression +
			", status=" + status +
			", description=" + description +
			", className=" + className +
			", methodName=" + methodName +
			"}";
	}
}
