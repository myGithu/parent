package com.yao.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yao
 * @since 2018-02-15
 */
public class Depts extends Model<Depts> {

    private static final long serialVersionUID = 1L;

	@TableId(value="deptId", type= IdType.AUTO)
	private Integer deptId;
	private String deptNo;
	private String dName;
	private String createTime;


	public Integer getDeptId() {
		return deptId;
	}

	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}

	public String getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(String deptNo) {
		this.deptNo = deptNo;
	}

	public String getdName() {
		return dName;
	}

	public void setdName(String dName) {
		this.dName = dName;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	@Override
	protected Serializable pkVal() {
		return this.deptId;
	}

	@Override
	public String toString() {
		return "Depts{" +
			"deptId=" + deptId +
			", deptNo=" + deptNo +
			", dName=" + dName +
			", createTime=" + createTime +
			"}";
	}
}
