package com.yao.contoller;

import com.yao.core.result.Msg;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by ruijie on 2018/2/16.
 */
@Controller
public class IndexController {

    @RequestMapping("/login2")
    public String toLogin2(){
        return "/login2";
    }


    @RequestMapping("/toIndex")
    public ModelAndView index(){
        ModelAndView mv = new ModelAndView("/springsecurity");
        Msg msg = new Msg("测试标题","测试内容","额外信息只对管理员显示");
        mv.addObject("msg",msg);
        return mv;
    }
}
