package com.yao.contoller;


import com.yao.core.result.Result;
import com.yao.entity.Schedule;
import com.yao.service.IScheduleService;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yao
 * @since 2018-02-18
 */
@Controller
@RequestMapping("/schedule")
public class ScheduleController {

    @Autowired
    private IScheduleService iScheduleService;

    @GetMapping("/addSchedule")
    public Result addSchedule(Schedule schedule){
        try {
            iScheduleService.addScheduleJob(schedule);
            return Result.SUCCESS;
        } catch (SchedulerException e) {
            return Result.ERROR;
        }
    }
}
