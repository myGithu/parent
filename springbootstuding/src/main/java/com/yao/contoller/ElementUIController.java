package com.yao.contoller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/elementui")
public class ElementUIController {

    @GetMapping("toHolleWord")
    public String toHolleWord(){
        return  "/elementui/holleword";
    }
}
