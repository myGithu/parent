package com.yao.contoller;


import com.yao.core.result.Msg;
import com.yao.core.result.Result;
import com.yao.core.util.DateTimeUtil;
import com.yao.entity.Users;
import com.yao.service.IUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author yao
 * @since 2018-01-31
 */
@Controller
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private IUsersService usersService;

    @ResponseBody
    @PostMapping("/saveUsers")
    public Users saveUsers(@ModelAttribute("user") Users user) {
        return usersService.saveUsers(user);
    }

    @ResponseBody
    @PostMapping("/removeUsers")
    public Result removeUsers(@RequestParam("userNo") String userNo) {
        return usersService.removeUsersById(userNo) ? Result.SUCCESS : Result.ERROR;
    }

    @ResponseBody
    @GetMapping("getUsersById/{userNo}")
    public Users getUsersById(@PathVariable("userNo") String userNo) {
        return usersService.getUsersById(userNo);
    }


}
