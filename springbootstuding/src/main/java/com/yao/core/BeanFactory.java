package com.yao.core;


import java.util.function.Supplier;

/**
 * Created by ruijie on 2018/1/5.
 */
public interface BeanFactory {

   public static <T> T createBean(Supplier<T> supplier){
       return supplier.get();
   }
 }

