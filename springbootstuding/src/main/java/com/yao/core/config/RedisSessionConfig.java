package com.yao.core.config;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * redis Session代替spring Session
 * 实现Session简单共享
 */
@Configurable
@EnableRedisHttpSession
public class RedisSessionConfig {

}
