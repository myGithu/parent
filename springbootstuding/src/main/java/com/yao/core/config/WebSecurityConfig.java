package com.yao.core.config;

import com.yao.service.impl.CustomUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Created by ruijie on 2018/1/23.
 * 继承WebSecurityConfigurerAdapter类时无需@EnableWebSecurity注解
 */
@Configuration
//@EnableWebSecurity
public class WebSecurityConfig
        extends WebSecurityConfigurerAdapter {

    // @Autowired
    // private DataSource dataSource;

    @Autowired
    CustomUserService customUserService;

    private final static String USER_NAME_QUERY = "SELECT username,password,true FROM myusers where username = ?";

    private final static String USER_AUTHORITIES_QUERY = "SELECT username, role from roles where username = ?";

    private static final String[] CLASSPATH_RESOURCE_LOCATIONS = {
            "classpath:/META-INF/resources/", "classpath:/resources/",
            "classpath:/static/", "classpath:/public/"};

    private static final  String[] RESOURCE_LOCATIONS = {"/errors/**","/js/**","/images/**", "/webjars/**","/css/**"};

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /**
         * antMatchers：ant风格路径匹配
         * permitAll：用户可以任意访问
         * anyRequest ： 匹配所有请求路径
         * access : spring el表达式问true时可以访问
         * anonymous ：匿名可以访问
         * denyAll ： 用户不能访问
         * fullAuthority:用户完全认证可以访问（非remember me下的自动登录）
         * hasAnyAuthority:如果用户有参数 ，则其中任意权限可以访问
         * hasAnyRole:如果用户有参数 ,则用户任意角色可以访问
         * hasAuthority:如果用户有参数 则其权限可以访问
         * hasIpAddress:如果用户来自参数中的Ip可以访问
         * rememberMe: 允许用过rememberMe登录的用户进行访问
         * authentucated:用户登录后可以访问
         */
        http.authorizeRequests()//开启请求权限配置
                .antMatchers(RESOURCE_LOCATIONS).permitAll()
                .antMatchers("/", "/login2").permitAll()//对/ 和 /login 不进行拦截
                .anyRequest()
                .authenticated()
                .and()
                .formLogin().loginPage("/login2")//设置登录访问路径为 /login
                .defaultSuccessUrl("/toIndex")//登录成功后转向默认成功路径/tochat
                .failureUrl("/errors/500.html")//登录失败后跳转的页面
                .permitAll()
                .and()
                //.rememberMe().tokenValiditySeconds(1209600).key("myKey")
               // .and()
                .logout().permitAll();

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //在内存中分配两个用户角色是：USER  spring Security 5 必须设置
        // .passwordEncoder(PasswordEncoderFactories.createDelegatingPasswordEncoder()) 密码编码
        // 在设置密码时必须{编码类型} + 密码编码后的字符串
        // 比如：{MD5}9852f6575c282db621d991fd9ced3cc1（ps:后面是密码MD5加密后的字符串）
       /* auth.inMemoryAuthentication()//添加内存中的用户
                .passwordEncoder(PasswordEncoderFactories.createDelegatingPasswordEncoder())
                .withUser("yao").password("{bcrypt}" + new BCryptPasswordEncoder().encode("yao")).roles("ADMIN")
                .and()
                .withUser("zt").password("{bcrypt}" + new BCryptPasswordEncoder().encode("zt")).roles("USER")*/
        ;
        //通过指定数据源来自定义用户查询和权限查询
        // auth.jdbcAuthentication().dataSource(dataSource).usersByUsernameQuery(USER_NAME_QUERY).authoritiesByUsernameQuery(USER_AUTHORITIES_QUERY);
        //通过自定义UserDetailsService来实现用户授权
        auth.userDetailsService(customUserService);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        //这些目录下的静态资源不拦截
        web.ignoring().antMatchers(CLASSPATH_RESOURCE_LOCATIONS);
    }
}
