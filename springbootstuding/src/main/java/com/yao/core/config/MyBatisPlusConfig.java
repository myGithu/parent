package com.yao.core.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import com.yao.core.config.annotation.MybatisPlusConfiguration;
import com.yao.core.properties.datasource.DruidProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;


/**
 * Created by ruijie on 2017/12/21.
 * 配置MyBatisPlus
 */
@MybatisPlusConfiguration(basePackages = "com.yao.dao")
public class MyBatisPlusConfig{

    @Autowired
    private DruidProperties druidProperties;



    private DruidDataSource getDataSource(){
        DruidDataSource dataSource = new DruidDataSource();
        druidProperties.config(dataSource);
        return dataSource;
    }

    @Bean
    public DataSource dataSource(){
        return getDataSource();
    }

    @Bean
    public PaginationInterceptor paginationInterceptor(){
        return new PaginationInterceptor();
    }
}
