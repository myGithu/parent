package com.yao.core.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

/**
 * Created by ruijie on 2018/1/23.
 */
@Configuration
@EnableWebSocketMessageBroker//通过这个注解开启使用STOMP协议来传输基于代理的消息，这个时候控制使用MessageMapping就和使用RequestMapping一样
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer{
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {//注册STOMP的节点，并映射到指定的URL
        registry.addEndpoint("/endpointWisely").withSockJS(); //注册一个STOMP的addEndpoint并指定使用withSockJS
        registry.addEndpoint("/endpointChat").withSockJS(); //注册一个STOMP的addEndpoint并指定使用withSockJS

    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) { //配置消息代理

        /**
         * 广播式应该配置一个/topic消息代理;
         * 点对点应增加一个/queue的消息代理
         */
        registry.enableSimpleBroker("/queue","/topic");
    }
}
