package com.yao.core.config.annotation;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.AliasFor;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.lang.annotation.*;

/**
 * Created by ruijie on 2017/12/28.
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Configuration
@MapperScan
@EnableTransactionManagement
public @interface MybatisPlusConfiguration {

    String[] value() default {};

    @AliasFor(annotation = MapperScan.class)
    String[] basePackages() default {};
}
