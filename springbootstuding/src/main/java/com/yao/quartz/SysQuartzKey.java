package com.yao.quartz;

/**
 * 定时任务公用Key值
 * Created by ruijie on 2018/2/19.
 */
public interface SysQuartzKey {
     static  final String JOB_PARAM_KEY = "schedule";
}
