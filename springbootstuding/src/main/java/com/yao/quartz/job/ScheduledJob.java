package com.yao.quartz.job;

import com.yao.entity.Schedule;
import com.yao.quartz.SysQuartzKey;
import lombok.extern.log4j.Log4j2;
import org.quartz.*;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by ruijie on 2018/2/18.
 * 业务执行工厂
 */
@Log4j2
public class ScheduledJob implements Job {

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        try {
            JobDetail jobDetail = context.getJobDetail();
            JobKey jobKey = jobDetail.getKey();
            JobDataMap jobDataMap = jobDetail.getJobDataMap();
            //定时器对象
            Schedule schedule = (Schedule) jobDataMap.get(SysQuartzKey.JOB_PARAM_KEY);
            /*根据类名与方法名称调用定时业务*/
            MethodInvokingJobDetailFactoryBean methodInvokingJobDetailFactoryBean= new MethodInvokingJobDetailFactoryBean();
            methodInvokingJobDetailFactoryBean.setTargetClass(Class.forName(schedule.getClassName()));
            methodInvokingJobDetailFactoryBean.setTargetMethod(schedule.getMethodName());
            methodInvokingJobDetailFactoryBean.prepare();
            methodInvokingJobDetailFactoryBean.invoke();
        } catch (ClassNotFoundException e) {
           log.error("没有发现执行类",e);
        } catch (NoSuchMethodException e) {
            log.error("没有发现执行方法",e);
        } catch (IllegalAccessException e) {
            log.error("参数传入有误",e);
        } catch (InvocationTargetException e) {
            log.error("方法调用有误",e);
        }
    }

}
