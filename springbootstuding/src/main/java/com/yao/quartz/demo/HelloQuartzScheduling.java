package com.yao.quartz.demo;

import com.yao.quartz.demo.HelloQuartzJob;
import org.quartz.*;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;

import java.text.ParseException;

/**
 * Created by ruijie on 2018/2/18.
 * 调度任务的调度器
 */
public class HelloQuartzScheduling {
    public static void main(String[] args) throws SchedulerException, ParseException {
        SchedulerFactory schedulerFactory = new StdSchedulerFactory();
        Scheduler scheduler = schedulerFactory.getScheduler();

        JobDetailImpl jobDetail = new JobDetailImpl();
        jobDetail.setName("helloQuartzJob");
        jobDetail.setGroup(Scheduler.DEFAULT_GROUP);
        jobDetail.setJobClass(HelloQuartzJob.class);

       /* SimpleTriggerFactoryBean simpleTriggerFactoryBean =  new SimpleTriggerFactoryBean();
        simpleTriggerFactoryBean.setJobDetail(jobDetail);
        simpleTriggerFactoryBean.setName("simpleTriggerImplm");
        simpleTriggerFactoryBean.setGroup(Scheduler.DEFAULT_GROUP);
        simpleTriggerFactoryBean.setStartTime(new Date(System.currentTimeMillis()));
        simpleTriggerFactoryBean.setRepeatInterval(5000L);
        simpleTriggerFactoryBean.setRepeatCount(10);
        simpleTriggerFactoryBean.afterPropertiesSet();*/

        CronTriggerFactoryBean cronTriggerFactoryBean = new CronTriggerFactoryBean();
        cronTriggerFactoryBean.setJobDetail(jobDetail);
        cronTriggerFactoryBean.setName("cronTriggerFactoryBean");
        cronTriggerFactoryBean.setCronExpression("0/5 * * * * ?");
        cronTriggerFactoryBean.setGroup(Scheduler.DEFAULT_GROUP);
        cronTriggerFactoryBean.afterPropertiesSet();
        scheduler.scheduleJob(jobDetail,cronTriggerFactoryBean.getObject());
        scheduler.start();
    }
}
