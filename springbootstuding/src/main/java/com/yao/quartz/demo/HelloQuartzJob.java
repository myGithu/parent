package com.yao.quartz.demo;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.time.LocalDateTime;

/**
 * Created by ruijie on 2018/2/18.
 * 需要被调度的任务
 */
public class HelloQuartzJob implements Job{
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("Hello, Quartz ! - execute its Job at "
                + LocalDateTime.now() + "By" + jobExecutionContext.getJobDetail().toString());
    }
}
