package com.yao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * Created by ruijie on 2017/12/26.
 */
@SpringBootApplication
@EnableCaching
public class SpringBootActionMian {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootActionMian.class,args);
    }
}
