package com.yao.books.springbootaction.diyizhang;

import com.yao.core.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Date;

/**
 * Created by ruijie on 2017/12/26.
 */
public class Main {
    public static void main(String[] args) {
       // ApplicationContext context =  new AnnotationConfigApplicationContext(DiConfig.class);
       // ApplicationContext context =  new AnnotationConfigApplicationContext(JavaConfig.class);
       // UseFunctionService useFunctionService = context.getBean(UseFunctionService.class);
       // System.out.println(useFunctionService.sayHello("word"));
        ApplicationContext context = new AnnotationConfigApplicationContext(AopConfig.class);
        DemoAnnotationService demoAnnotationService = context.getBean(DemoAnnotationService.class);
        demoAnnotationService.add();
        DemoMethodService demoMethodService = context.getBean(DemoMethodService.class);
        demoMethodService.add();

       String s =   BeanFactory.createBean(String :: new);
        Date date = BeanFactory.createBean(Date::new);
    }
}
