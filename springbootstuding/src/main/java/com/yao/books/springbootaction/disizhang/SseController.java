package com.yao.books.springbootaction.disizhang;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

/**
 * Created by ruijie on 2017/12/29.
 */
@Controller
public class SseController {

    /**
     * text/event-stream 使用SSE方式进行服务器推送设置的媒体类型
     * @return
     */
    @GetMapping(value = "/push",produces = "text/event-stream;charset=UTF-8")
    public String push(){
        Random r = new Random();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "data:Testing 1,2,3 " + r.nextInt() + "\n\n";
    }
}
