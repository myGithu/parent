package com.yao.books.springbootaction.diyizhang;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by ruijie on 2017/12/26.
 */
@Configuration
public class JavaConfig {

    @Bean
    public FunctionService functionService(){
        return  new FunctionService();
    }

    @Bean
    public UseFunctionService useFunctionService(){
        UseFunctionService useFunctionService = new UseFunctionService();
        useFunctionService.setFunctionService(functionService());
        return useFunctionService;
    }
}
