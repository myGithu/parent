package com.yao.books.springbootaction.diyizhang;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by ruijie on 2017/12/26.
 */
//@Service
public class UseFunctionService {

   // @Autowired
    private FunctionService functionService;

    public void setFunctionService(FunctionService functionService) {
        this.functionService = functionService;
    }

    public String sayHello(String word){
        return functionService.sayHello(word);
    }
}
