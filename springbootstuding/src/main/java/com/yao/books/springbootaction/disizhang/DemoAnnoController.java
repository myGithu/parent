package com.yao.books.springbootaction.disizhang;


import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ruijie on 2017/12/28.
 */
@RestController
@RequestMapping("/anno")
public class DemoAnnoController {

    @GetMapping
    public Map<String,String> index(HttpServletRequest request){
        Map map = new HashMap();
        map.put("url",request.getLocalAddr());
        return map;
    }

    @GetMapping("{str}")
    public Map<String,String> demoPathVar(@PathVariable("str") String str, HttpServletRequest request){
        Map map = new HashMap();
        map.put("url",request.getLocalAddr());
        map.put("str",str);
        return map;
    }

    /**
     * 不同的路径映射到相同的方法
     * @param str1
     * @param str2
     * @param request
     * @return
     */
    @GetMapping(value = {"/str1/{str1}/{str2}","/str2/{str1}/{str2}"})
    public Map<String,String> remove(@PathVariable("str1") String str1,@PathVariable("str2") String str2, HttpServletRequest request){
        Map map = new HashMap();
        map.put("url",request.getLocalAddr());
        map.put("str1",str1);
        map.put("str2",str2);
        return map;
    }

    @RequestMapping("/getparam")
    public Map<String,String> getparam(String str, HttpServletRequest request){
        Map map = new HashMap();
        map.put("url",request.getLocalAddr());
        map.put("str",str);
        return map;
    }
}
