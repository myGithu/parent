package com.yao.books.springbootaction.diyizhang;

import org.springframework.stereotype.Service;

/**
 * Created by ruijie on 2017/12/26.
 */
@Service
public class DemoAnnotationService {
    @Action(name = "注解拦截的Add操作")
    public void add(){
        System.out.println("你好");
    }
}
