package com.yao.books.springbootaction.disanzhang;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

/**
 * Created by ruijie on 2017/12/27.
 */
@Configuration
public class ConditionConfig {

  /* @Bean("windowsCondition")
    public Condition windowsCondition(){
        return (context,metadata) -> context.getEnvironment().getProperty("os.name").contains("Windows");
    }

    @Bean("linuxCondition")
    public  Condition linuxCondition(){
        return (context,metadata) -> context.getEnvironment().getProperty("os.name").contains("Linux");
    }*/


    @Bean("windowsListService")
    @Conditional(WindowsCondition.class)
    public ListService windowsListService(){
        return () -> "dir";
    }

    @Bean("linuxListService")
    @Conditional(LinuxCondition.class)
    public ListService linuxListService(){
        return () -> "ls";
    }

  /* @Bean
   @Conditional(WindowsCondition.class)
   public ListService windowsListService(){
       return new WindowsListService();
   }

   @Bean
   @Conditional(LinuxCondition.class)
   public ListService linuxListService(){
       return new LinuxListService();
   }*/


}


