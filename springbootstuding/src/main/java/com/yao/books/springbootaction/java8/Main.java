package com.yao.books.springbootaction.java8;

import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.Callable;

/**
 * Created by ruijie on 2018/1/5.
 */
public class Main {
    public static void main(String[] args) {
        //Optional<String> fullName = Optional.ofNullable(null);
        //System.out.println("Full Name is set ?" + fullName.isPresent());
        //System.out.println("Full Name :" + fullName.orElseGet(() ->"[none]"));
        //System.out.println(fullName.map(s -> "Hey " + s  + "!").orElse("Hey Stranger!"));
        /*int a = 50;
        ArrayList<Integer> b = new ArrayList<Integer>(){{
            add(1);
            add(2);
            add(3);
            add(4);
        }};
        set(a,b);
        System.out.println("a:" + a + "=== b:" + b);
        Callable<Integer> callable = () -> 1;*/
        Runnable  runnable = () -> System.out.println("x");
        new Thread(runnable).start();


    }

    static void set(int a, ArrayList<Integer> b){
        a = a + 10;
        b.add(30);
        System.out.println("a:" + a + "=== b:" + b);
    }
}


