package com.yao.books.springbootaction.disizhang;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

/**
 * Created by ruijie on 2017/12/29.
 */
@RestController
public class AysncController {
    @Autowired
    private PushService pushService;

    @RequestMapping(value = "/defer", produces = "text/plain;charset=UTF-8")
    public DeferredResult<String> deferredResult(){
        return pushService.getDeferredResult();
    }
}
