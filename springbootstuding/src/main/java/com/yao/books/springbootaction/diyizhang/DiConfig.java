package com.yao.books.springbootaction.diyizhang;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by ruijie on 2017/12/26.
 */
@Configuration
@ComponentScan("com.yao.books.springbootaction.diyizhang")
public class DiConfig {
}
