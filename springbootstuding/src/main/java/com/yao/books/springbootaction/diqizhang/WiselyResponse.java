package com.yao.books.springbootaction.diqizhang;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by ruijie on 2018/1/23.
 */
@AllArgsConstructor
public class WiselyResponse {
    @Getter
    private String responseMessage;

}
