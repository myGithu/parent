package com.yao.books.springbootaction.java8;

import lombok.Data;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.nio.charset.StandardCharsets;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Function;
import java.util.function.IntBinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by ruijie on 2018/1/5.
 */
public class Streams {

    private int age;

    public Streams() {
    }

    public Streams(int age) {
        this.age = age;
    }

    private enum Status {
        OPEN, CLOSED
    }

    @Data
    private static final class Task {
        private final Status status;

        private final Integer points;
    }

    static final Collection<Task> tasks = Arrays.asList(
            new Task(Status.OPEN, 5),
            new Task(Status.OPEN, 13),
            new Task(Status.CLOSED, 8)
    );

    public static int sum(int a, int b) {
        return a + b;
    }

    interface X<T>{

       T  y();

    }

    @FunctionalInterface
    interface XV2<T,K>{

        T  y(K k);

    }
    public static boolean isNumericZidai(String str) {
        for (int i = 0; i < str.length(); i++) {
            System.out.println(str.charAt(i));
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) throws ScriptException {
        Function<Integer,Streams> function =   Streams :: new;
        Streams streams = function.apply(2);
        final IntBinaryOperator i =  Streams :: sum;
        final int total1 = tasks.stream()
                .filter(task -> task.getStatus() == Status.OPEN || task.getStatus() == Status.CLOSED)
                .map(Task::getPoints)
                .reduce(Integer :: sum).get();
        System.out.println(total1);

     /*  final double total2 = tasks.parallelStream()
                .map(Task::getPoints)
                .reduce(0, Integer::sum);
        System.out.println(total2);


        Map<Status, List<Task>> map = tasks.stream().collect(Collectors.groupingBy(Task::getStatus));
        System.out.println(map);

        final Clock clock = Clock.systemDefaultZone();
        System.out.println("clock" + clock);
        System.out.println(Clock.systemUTC());
        System.out.println(clock.instant());
        System.out.println(clock.millis());
        System.out.println(Instant.now());
        System.out.println(LocalTime.now());
        System.out.println(LocalDateTime.now().getMinute());
        System.out.println(System.currentTimeMillis());
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName( "JavaScript" );
        System.out.println(engine.eval("function f(){" +
                "return 1} f() + 1"));

        String text = "Base64 final in java 8";
        *//*Base64编码*//*
        final String encoded = Base64.getEncoder().encodeToString(text.getBytes(StandardCharsets.UTF_8));

        *//*Base64解码*//*
        final String decoed = new String(Base64.getDecoder().decode(encoded),StandardCharsets.UTF_8);
        System.out.println(encoded);
        System.out.println(decoed);

        long[] arrayOfLong = new long [ 20000 ];
        Arrays.parallelSetAll(arrayOfLong,index -> ThreadLocalRandom.current().nextInt( 1000000 ));
        Arrays.stream(arrayOfLong).limit(10).forEach(i-> System.out.print(i + " "));

        System.out.println();
        Arrays.parallelSort( arrayOfLong );
        Arrays.stream( arrayOfLong ).limit( 10 ).forEach(
        i -> System.out.print( i + " " ) );
        System.out.println();*/
    }


}
