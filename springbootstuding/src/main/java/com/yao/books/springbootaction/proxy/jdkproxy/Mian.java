package com.yao.books.springbootaction.proxy.jdkproxy;


public class Mian {
    public static void main(String[] arg) {
       MyProxyImpl myProxy = new MyProxyImpl();
       /* MyProxyImpl myProx = new MyProxyImpl();

        MyProxy myProxy1 = (MyProxy) Proxy.newProxyInstance(myProxy.getClass().getClassLoader(),myProxy.getClass().getInterfaces(),(proxy, method, args) ->{
            Object reslut =    method.invoke(myProxy,args);
            System.out.println("befor---------------");
            System.out.println("after----------");

            return reslut;

        });

        myProxy1.print();*/

        MyProxy myProxy1 = (MyProxy) ProxyFactory.getProxyInstance(myProxy,Aop.getInstance());
        myProxy1.print();
    }
}
