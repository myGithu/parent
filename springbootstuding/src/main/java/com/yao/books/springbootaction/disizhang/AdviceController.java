package com.yao.books.springbootaction.disizhang;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by ruijie on 2017/12/29.
 */
@Controller
public class AdviceController {

    @RequestMapping("/advice")
    public String getSomething(@ModelAttribute("msg") String msg) throws IllegalAccessException {
            throw  new IllegalAccessException("非常抱歉，参数有误/" + "来自ModelAttribute：" + msg);
    }
}
