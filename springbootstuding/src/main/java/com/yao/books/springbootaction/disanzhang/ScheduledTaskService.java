package com.yao.books.springbootaction.disanzhang;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalTime;

/**
 * Created by ruijie on 2017/12/26.
 */
@Service
public class ScheduledTaskService {

   // @Scheduled(fixedRate = 5000)
    public void reportCurrentTime(){
        System.out.println("每隔五秒执行一次" + LocalTime.now());
    }

   // @Scheduled(cron = "0 8 17 ? * *")
    public void fixTimeExecution(){
        System.out.println("在指定时间 " + LocalTime.now() + "执行");
    }
}
