package com.yao.books.springbootaction.dierzhang;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * Created by ruijie on 2017/12/26.
 */
@Component
public class DemoListener implements ApplicationListener<DemoEvent> {
    @Override
    public void onApplicationEvent(DemoEvent event) {
        String msg = event.getMsg();
        System.out.println("我（bean-demoListener）接受到了bean-demoPublisher发布的消息：" + msg);
    }
}
