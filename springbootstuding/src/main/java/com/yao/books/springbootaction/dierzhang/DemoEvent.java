package com.yao.books.springbootaction.dierzhang;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

/**
 * Created by ruijie on 2017/12/26.
 */
public class DemoEvent extends ApplicationEvent{

    private static final long serialVersionUID = 1L;
    @Getter
    @Setter
    private String msg;

    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */

    public DemoEvent(Object source,String msg) {
        super(source);
        this.msg = msg;
    }
}
