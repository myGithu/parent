package com.yao.books.springbootaction.proxy.cglibproxy;

import com.yao.books.springbootaction.proxy.jdkproxy.Aop;
import com.yao.books.springbootaction.proxy.jdkproxy.MyProxy;
import com.yao.books.springbootaction.proxy.jdkproxy.MyProxyImpl;

public class Main {
    public static void main(String[] args) {
        MyProxy myProxy = (MyProxy) new ProxyFactory(MyProxyImpl.getInstance(), Aop.getInstance()).getProxyInstance();
        myProxy.print();
    }
}
