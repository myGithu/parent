package com.yao.books.springbootaction.diqizhang;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by ruijie on 2018/1/12.
 */
@Controller
public class ThymelealController {

    @GetMapping("/person")
    public ModelAndView tothymeleaftest(){
        ModelAndView mv = new ModelAndView("/thymeleafTest");
        mv.addObject("person",Person.person);
        return mv;
    }
    @GetMapping("/persons")
    public ModelAndView persons(){
        ModelAndView mv = new ModelAndView("/thymeleafTest");
        mv.addObject("persons",Person.persons);
        return mv;
    }
}
