package com.yao.books.springbootaction.disizhang;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ruijie on 2017/12/28.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DemoObj {
    private Long id;

    private String name;
}
