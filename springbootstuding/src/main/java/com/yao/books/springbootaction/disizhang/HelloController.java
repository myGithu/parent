package com.yao.books.springbootaction.disizhang;

import com.yao.core.config.SystemConfig;
import com.yao.core.result.Result;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * Created by ruijie on 2017/12/28.
 */
@Log4j2
@Controller
public class HelloController {

    @ResponseBody
    @PostMapping("/fileupload")
    public Result fileupload(@RequestParam("file")MultipartFile file) {
        try {
            FileUtils.writeByteArrayToFile(new File(SystemConfig.FILE_PATH + file.getOriginalFilename()),file.getBytes());
            return Result.SUCCESS;
        } catch (IOException e) {
            return Result.ERROR;
        }

    }

    @GetMapping("/download/{fileName}")
    public ResponseEntity<byte[]> fileDownload(@PathVariable("fileName") String fileName){
        try {
            log.info("---------------------" + fileName + "--------------------------");
            byte[] fileBytes =  FileUtils.readFileToByteArray(new File(SystemConfig.FILE_PATH + fileName));
            HttpHeaders headers = new HttpHeaders();
            headers.setContentDispositionFormData("attachment",fileName);
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            return new ResponseEntity<byte[]>(fileBytes,headers, HttpStatus.CREATED);
        } catch (IOException e) {
            e.printStackTrace();
            return  null;
        }
    }
}
