package com.yao.books.springbootaction.dierzhang;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by ruijie on 2017/12/26.
 */
public class Main {
    public static void main(String[] args) {
      /*  ApplicationContext context = new AnnotationConfigApplicationContext(ScopeConfig.class);
        DemoSingletonService demoSingletonService1 =  context.getBean(DemoSingletonService.class);
        DemoSingletonService demoSingletonService2 = context.getBean(DemoSingletonService.class);

        DemoPrototypeService demoPrototypeService1 = context.getBean(DemoPrototypeService.class);
        DemoPrototypeService demoPrototypeService2 = context.getBean(DemoPrototypeService.class);

        System.out.println("demoSingletonService1与demoSingletonService2是否相等："
                + demoSingletonService1.equals(demoSingletonService2)
        );
        System.out.println("demoPrototypeService1与demoPrototypeService2是否相等："
                + demoPrototypeService1.equals(demoPrototypeService2)
        );*/

        // ApplicationContext context = new AnnotationConfigApplicationContext(ELConfig.class);
        // ELConfig elConfig =  context.getBean(ELConfig.class);
        // elConfig.outputResouce();
        //ApplicationContext context = new AnnotationConfigApplicationContext(PrePostConfig.class);
        //BeanWayService beanWayService = context.getBean(BeanWayService.class);
        //JSR250WayService jsr250WayService = context.getBean(JSR250WayService.class);

        ApplicationContext context = new AnnotationConfigApplicationContext(EvenConfig.class);
        DemoPublisher demoPublisher = context.getBean(DemoPublisher.class);
        demoPublisher.publish("什么鬼！");
    }
}
