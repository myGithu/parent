package com.yao.books.springbootaction.disanzhang;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * Created by ruijie on 2017/12/28.
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Configuration
@ComponentScan
public @interface WiselyConfiguration {

    @AliasFor(annotation = ComponentScan.class)
    String[]  value() default {};
}
