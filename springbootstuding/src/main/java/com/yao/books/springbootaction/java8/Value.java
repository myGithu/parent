package com.yao.books.springbootaction.java8;

/**
 * Created by ruijie on 2018/1/5.
 */
public class Value<T> {
    public static <T> T defaultValue(){
        return null;
    }

    public T getOrDefault(T value,T defaultValue){
        return value != null ? value : defaultValue;
    }

}
class    TypeInference {
    public static void main(String[] args) {
        final Value<String> value = new Value<>();
        value.getOrDefault("22",Value.defaultValue());
    }
}
