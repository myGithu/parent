package com.yao.books.springbootaction.proxy.jdkproxy;

public class Aop {
    public void befor(){
        System.out.println("befor");
    }


    public void after(){
        System.out.println("after");
    }

    public static Aop getInstance(){
        return new Aop();
    }
}
