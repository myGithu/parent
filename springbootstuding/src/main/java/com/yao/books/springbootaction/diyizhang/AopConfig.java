package com.yao.books.springbootaction.diyizhang;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Created by ruijie on 2017/12/26.
 */
@Configuration
@ComponentScan("com.yao.books.springbootaction.diyizhang")
@EnableAspectJAutoProxy//开启spring 对AspectJ的支持
public class AopConfig {
}
