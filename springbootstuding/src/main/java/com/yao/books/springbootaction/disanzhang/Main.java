package com.yao.books.springbootaction.disanzhang;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by ruijie on 2017/12/26.
 */
public class Main {
    public static void main(String[] args) {
        //ApplicationContext context = new AnnotationConfigApplicationContext(AwareConfig.class);
        //AwareService awareService =  context.getBean(AwareService.class);
        // awareService.outputResult();
       /* ApplicationContext context = new AnnotationConfigApplicationContext(TaskExecutorConfig.class);
        AsyncTaskService asyncTaskService = context.getBean(AsyncTaskService.class);
        for (int i = 0; i < 10; i++) {
            asyncTaskService.executeAsyncTask(i);
            asyncTaskService.executeAsyncTaskPlus(i);
        }*/
        //ApplicationContext context = new AnnotationConfigApplicationContext(TaskSchedulerConfig.class);
        //AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConditionConfig.class);
        //context.getBean(Condition.class);
      //  ListService listService = (ListService) context.getBean("linuxListService");
      //  ListService listService = context.getBean(ListService.class);
       // System.out.println(context.getEnvironment().getProperty("os.name") + "系统下的命令为： " + listService.showListCmd());
        ApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);
        context.getBean(DemoService1.class).outputResult();

    }
}
