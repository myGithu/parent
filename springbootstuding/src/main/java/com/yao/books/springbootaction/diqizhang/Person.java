package com.yao.books.springbootaction.diqizhang;

import com.alibaba.fastjson.annotation.JSONField;
import com.yao.core.util.DateTimeUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ruijie on 2018/1/12.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person {

        private String name;

        private Integer age;

        private String address;

        @JSONField(serialize = false)
        private String createDateTime;

        static Person person =  new Person("张三",10);

        public Person(String name, Integer age) {
                this.name = name;
                this.age = age;
        }

        static List persons = Arrays.asList(
                new Person("张三",10),
                new Person("李四",11),
                new Person("王五",12),
                new Person("赵六",13),
                new Person("姚松兵",18)
        );

}
