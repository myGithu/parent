package com.yao.books.springbootaction.dierzhang;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * Created by ruijie on 2017/12/26.
 */
@Service
@Scope("prototype")
public class DemoPrototypeService {
}
