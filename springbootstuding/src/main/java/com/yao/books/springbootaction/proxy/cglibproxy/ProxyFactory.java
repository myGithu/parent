package com.yao.books.springbootaction.proxy.cglibproxy;

import com.yao.books.springbootaction.proxy.jdkproxy.Aop;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class ProxyFactory implements MethodInterceptor{

    private Object target;
    private Aop aop;
    public ProxyFactory(Object target, Aop aop) {
        this.target = target;
        this.aop = aop;
    }

    public Object getProxyInstance(){
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(target.getClass());
        enhancer.setCallback(this);
        return enhancer.create();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        aop.befor();
        Object reslut = method.invoke(target,objects);
        aop.after();
        return reslut;
    }
}
