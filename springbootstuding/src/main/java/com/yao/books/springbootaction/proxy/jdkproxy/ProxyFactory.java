package com.yao.books.springbootaction.proxy.jdkproxy;

import java.lang.reflect.Proxy;


public class ProxyFactory {
    private static Object target;

    private static Aop aop;

    public static Object getProxyInstance(Object _target, Aop _aop) {
        target = _target;
        aop = _aop;
        return Proxy.newProxyInstance(target.getClass().getClassLoader(),
                target.getClass().getInterfaces(),
                (proxy, method, args) -> {
                    aop.befor();
                    Object reslut = method.invoke(target, args);
                    aop.after();
                    return reslut;
                });
    }

}
