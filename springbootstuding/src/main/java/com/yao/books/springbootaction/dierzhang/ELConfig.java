package com.yao.books.springbootaction.dierzhang;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;


/**
 * Created by ruijie on 2017/12/26.
 */
//@Configuration
//@ComponentScan("com.yao.books.springbootaction.dierzhang")
//@PropertySource("classpath*:com\\yao\\books\\springbootaction\\dierzhang\\test.properties")
public class ELConfig {
    @Value("I Love You!")
    private String normal;

    @Value("#{systemProperties['os.name']}")
    private String osName;

    @Value("#{ T(java.lang.Math).random() *100}")
    private double randomNumber;

    @Value("#{demoService1.another}")
    private String fromAnother;


    @Value("${book.name}")
    private String bookName;

    @Autowired
    private Environment environment;


    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer(){
        return new PropertySourcesPlaceholderConfigurer();
    }

    public void outputResouce(){
        System.out.println(normal);
        System.out.println(osName);
        System.out.println(randomNumber);
        System.out.println(fromAnother);
        System.out.println(bookName);
        System.out.println(environment.getProperty("book.author"));
    }

}
