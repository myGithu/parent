package com.yao.books.springbootaction.disanzhang;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by ruijie on 2017/12/26.
 */
@Configuration
@ComponentScan("com.yao.books.springbootaction.disanzhang")
//@EnableScheduling//开启对定时任务的支持
public class TaskSchedulerConfig {
}
