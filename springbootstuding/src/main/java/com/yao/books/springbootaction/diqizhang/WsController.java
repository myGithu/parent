package com.yao.books.springbootaction.diqizhang;

import com.alibaba.fastjson.JSONArray;
import com.yao.core.result.Result;
import com.yao.core.util.DateTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Created by ruijie on 2018/1/23.
 */
@Controller
public class WsController {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate; //向浏览器发送消息

    /**
     * websocket 广播订阅
     *
     * @param message
     * @return
     * @throws InterruptedException
     */
    @MessageMapping("/welcome")
    @SendTo("/topic/getResponse")
    public WiselyResponse say(WiselyMessage message) throws InterruptedException {
        Thread.sleep(3000);
        return new WiselyResponse("Welcome," + message.getName() + "!");
    }

    @RequestMapping("/ws")
    public String toWs() {
        return "/ws";
    }

    /**
     * @param principal 该参数中包含当前用户的信息
     * @param msg
     */
    @MessageMapping("/chat")
    public void handleChat(Principal principal, String msg) {
        if (principal.getName().equals("yao")) {//判断发送人并把消息发送给相应的另一个人
            simpMessagingTemplate.convertAndSendToUser(//通过该方法向用户发送消息
                    "zt",//消息接收人
                    "/queue/notifications", //浏览器订阅的地址
                    principal.getName() + " -send:" + msg); //发送的消息
        } else {
            simpMessagingTemplate.convertAndSendToUser(
                    "yao",
                    "/queue/notifications",
                    principal.getName() + " -send:" + msg);
        }
    }

    @RequestMapping("/login")
    public String login() {
        return "/login";
    }

    @RequestMapping("/tochat")
    public String chat() {
        return "/chat";
    }

    @RequestMapping("/tobootstrap")
    public String tobootstrap() {
        return "/bootstrap";
    }

    @RequestMapping("/toangularjs")
    public String toAngularJs() {
        return "/angularjs";
    }

    @RequestMapping("/toaction")
    public String toAction(){
        return "/action";
    }

    @ResponseBody
    @RequestMapping("/search")
    public Person search(){
        return new Person("lisi",18,"shanghai", DateTimeUtil.getStringNowDateTime());
    }

    @PostMapping("/addPersons")
    @ResponseBody
    public Result addPersons(@RequestParam("persons") String persons){
        JSONArray jsonArray = JSONArray.parseArray(persons);
        List<Person> list =  jsonArray.toJavaList(Person.class);
        System.out.println(jsonArray);
        return  Result.SUCCESS;
    }
}
