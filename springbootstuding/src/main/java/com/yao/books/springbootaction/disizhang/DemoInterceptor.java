package com.yao.books.springbootaction.disizhang;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by ruijie on 2017/12/28.
 */
public class DemoInterceptor implements HandlerInterceptor {

    private final static Logger logger = LoggerFactory.getLogger(DemoInterceptor.class);

    /**
     * 预处理方法 返回true执行postHandle  返回false执行中断
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        long startTime = System.currentTimeMillis();
        request.setAttribute("startTime", startTime);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        long startTime = (long) request.getAttribute("startTime");
        request.removeAttribute("startTime");
        long endTime = System.currentTimeMillis();
        logger.info("本次请求处理时间为：" + (endTime - startTime) + " ms");
        request.setAttribute("handlingTime",endTime - startTime);
    }
}
