package com.yao.books.springbootaction.disizhang;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;

/**
 * Created by ruijie on 2017/12/29.
 */
@Service
public class PushService {
    private DeferredResult<String> deferredResult;

    public  DeferredResult getDeferredResult(){
        deferredResult = new DeferredResult<>();
        return deferredResult;
    }

    @Scheduled(fixedDelay = 5000)
    public void refresh(){
        if(deferredResult != null){
            deferredResult.setResult("李瑞花：" + new Long(System.currentTimeMillis()));
        }
    }

}
