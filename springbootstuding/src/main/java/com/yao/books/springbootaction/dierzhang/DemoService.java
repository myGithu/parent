package com.yao.books.springbootaction.dierzhang;

import lombok.Data;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Created by ruijie on 2017/12/26.
 */

@Data
@Service
public class DemoService {

        @Value("其他类的属性")
        private String another;
}
