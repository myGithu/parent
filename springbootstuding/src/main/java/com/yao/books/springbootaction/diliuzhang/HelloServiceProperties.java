package com.yao.books.springbootaction.diliuzhang;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by ruijie on 2018/1/12.
 */

@ConfigurationProperties(prefix = "hello")
public class HelloServiceProperties {
    private static final String MSG = "world";

    @Setter
    @Getter
    private String msg = MSG;

}
