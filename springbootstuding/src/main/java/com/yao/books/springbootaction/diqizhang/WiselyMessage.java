package com.yao.books.springbootaction.diqizhang;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ruijie on 2018/1/23.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WiselyMessage {
    private String name;
}
