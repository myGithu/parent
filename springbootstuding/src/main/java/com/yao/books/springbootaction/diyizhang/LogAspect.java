package com.yao.books.springbootaction.diyizhang;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * Created by ruijie on 2017/12/26.
 */
@Aspect
@Component
public class LogAspect {
    @Pointcut("@annotation(com.yao.books.springbootaction.diyizhang.Action)")
    public void annotationPointCut() {
    }

    @Around("annotationPointCut()")
    public void around(ProceedingJoinPoint joinPoint) {
        //System.out.println("nihao");
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();//获取封装了署名信息的对象,在该对象中可以获取到目标方法名,所属类的Class等信息
        Method method = signature.getMethod();
        Action action = method.getAnnotation(Action.class);
        System.out.println("注解式拦截" + action.name());
        try {
            joinPoint.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        System.out.println("注解式拦截" + action.name());

    }

    /**符号	含义
     *execution（）
     *表达式的主体；
     *第一个”*“符号
     *表示返回值的类型任意；
     *com.sample.service.impl	AOP所切的服务的包名，即，我们的业务部分
     *包名后面的”..“	表示当前包及子包
     *第二个”*“	表示类名，*即所有类。此处可以自定义，下文有举例
     *      .*(..)	表示任何方法名，括号表示参数，两个点表示任何参数类型
     *      */
    @Before("execution(* com.yao.books.springbootaction.diyizhang.DemoMethodService.*(..))")
    public void befor(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        System.out.println("方法式拦截：" + method.getName());
    }


}
