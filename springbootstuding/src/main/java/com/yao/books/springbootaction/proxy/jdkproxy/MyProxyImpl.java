package com.yao.books.springbootaction.proxy.jdkproxy;

public class MyProxyImpl implements MyProxy{
    @Override
    public void print() {
        System.out.println("niaho");
    }

    @Override
    public void test() {
        System.out.println("test");
    }

    public static MyProxy getInstance(){
        return new MyProxyImpl();
    }
}
