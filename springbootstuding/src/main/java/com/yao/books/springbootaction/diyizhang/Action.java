package com.yao.books.springbootaction.diyizhang;

import java.lang.annotation.*;

/**
 * Created by ruijie on 2017/12/26.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Action {
    String name();
}
