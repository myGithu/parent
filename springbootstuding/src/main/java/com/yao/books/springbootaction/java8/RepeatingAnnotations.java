package com.yao.books.springbootaction.java8;


import java.lang.annotation.*;

/**
 * Created by ruijie on 2018/1/5.
 */
public class RepeatingAnnotations {

    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface  Filters{
        Filter[] value() default {};
    }

    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @Repeatable(Filters.class)
    public @interface  Filter{
        String value();
    }


    @Filter("filter1")
    @Filter("filter2")
    public interface  Filterable{

    }


    public static void main(String[] args) {
        for (Filter filter : Filterable.class.getAnnotationsByType(Filter.class)) {
            System.out.println(filter.value());
        }
    }

}
