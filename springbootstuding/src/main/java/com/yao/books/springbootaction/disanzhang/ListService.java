package com.yao.books.springbootaction.disanzhang;

/**
 * Created by ruijie on 2017/12/27.
 */
@FunctionalInterface
public interface ListService {
    String showListCmd();
}
