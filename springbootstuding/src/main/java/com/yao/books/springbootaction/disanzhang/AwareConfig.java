package com.yao.books.springbootaction.disanzhang;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by ruijie on 2017/12/26.
 */
@Configuration
@ComponentScan("com.yao.books.springbootaction.disanzhang")
public class AwareConfig {
}
