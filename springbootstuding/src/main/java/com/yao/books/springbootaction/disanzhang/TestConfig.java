package com.yao.books.springbootaction.disanzhang;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Created by ruijie on 2017/12/28.
 */
@Configuration
public class TestConfig {

    @Bean
    @Profile("dev")
    public TestBean devTestBean(){
        return new TestBean();
    }

    @Bean
    @Profile("prod")
    public TestBean prodTestBean(){
        return new TestBean();
    }
}
