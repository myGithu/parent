package com.yao.books.springbootaction.disanzhang;

import lombok.Data;

/**
 * Created by ruijie on 2017/12/28.
 */

@Data
public class TestBean {
    private String content;
}
