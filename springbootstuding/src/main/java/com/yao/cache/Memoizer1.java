package com.yao.cache;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by ruijie on 2018/3/16.
 */
public class Memoizer1<K,V> implements Computable<K,V> {
    //private final Map<K,V> cache = new HashMap<>();
    private final Map<K,V> cache = new ConcurrentHashMap<>();
    private final Computable<K,V> c;

    public Memoizer1(Computable<K, V> c) {
        this.c = c;
    }

    @Override
    public V compute(K key) throws Exception {
        V value = cache.get(key);
        if (value == null){
            value = c.compute(key);
            cache.put(key,value);
        }
        return value;
    }

    public static void main(String[] args) {
        Computable computable = new Memoizer1<>(new UserCostStatComputer1());
        try {
            System.out.println(computable.compute("1"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
