package com.yao.cache;

import java.util.concurrent.*;

/**
 * Created by ruijie on 2018/3/16.
 */
public class Memoizer<K, V> implements Computable<K, V> {
    private final ConcurrentMap<K, Future<V>> cache = new ConcurrentHashMap<>();

    private final Computable<K, V> c;

    public Memoizer(Computable<K, V> c) {
        this.c = c;
    }

    @Override
    public V compute(final K key) throws Exception {
        while (true) {
            Future<V> f = cache.get(key);
            if (f == null) {
                Callable<V> callable = () -> c.compute(key);
                FutureTask<V> ft = new FutureTask<>(callable);
                f = cache.putIfAbsent(key, ft);

                if (f == null) {
                    f = ft;
                    ft.run();
                }
            }

            try {
                return f.get();
            } catch (CancellationException e) {
                cache.remove(key, f);
            } catch (ExecutionException e) {
                e.printStackTrace();
                throw e;
            }
        }
    }
}
