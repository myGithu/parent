package com.yao.cache;

import java.math.BigInteger;
import java.util.concurrent.TimeUnit;

/**
 * Created by ruijie on 2018/3/16.
 */
public class UserCostStatComputer1 implements Computable<String,BigInteger> {
    @Override
    public BigInteger compute(String key) throws Exception {
        TimeUnit.SECONDS.sleep(3);
        return new BigInteger(key);
    }
}
