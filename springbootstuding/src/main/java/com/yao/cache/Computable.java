package com.yao.cache;

/**
 * Created by ruijie on 2018/3/16.
 */
public interface Computable<K,V> {
    V compute(K key) throws Exception;
}
