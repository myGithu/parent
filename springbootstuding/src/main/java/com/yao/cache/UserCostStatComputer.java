package com.yao.cache;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by ruijie on 2018/3/16.
 */
public class UserCostStatComputer {
    private final Map<String,BigInteger> cache = new HashMap<>();

    public synchronized BigInteger conpute(String userId) throws InterruptedException {
        BigInteger result =  cache.get(userId);
        if (result == null){
            doCompute(userId);
            cache.put(userId,result);
        }
        return result;
    }

    private BigInteger doCompute(String userId) throws InterruptedException {
        TimeUnit.SECONDS.sleep(3);
        return new BigInteger(userId);
    }
}
