package com.yao.service;

import com.yao.entity.Users;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author yao
 * @since 2018-01-31
 */
public interface IUsersService extends IService<Users> {
    Users saveUsers(Users user);

    Boolean removeUsersById(String userNo);

    Users getUsersById(String userNo);

    Users getUserByName(String userName);

}
