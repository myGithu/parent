package com.yao.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.yao.entity.Schedule;
import com.baomidou.mybatisplus.service.IService;
import org.quartz.SchedulerException;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yao
 * @since 2018-02-18
 */
public interface IScheduleService extends IService<Schedule> {
    Schedule addScheduleJob(Schedule schedule) throws  SchedulerException;

    void startNowScheduleJob(Schedule schedule) throws SchedulerException;

    void stopScheduleJob(Schedule schedule) throws SchedulerException;

    void restartScheduleJob(Schedule schedule) throws SchedulerException;

    void modifyScheduleJob(Schedule schedule) throws SchedulerException;

    void removeScheduleJob(Schedule schedule) throws SchedulerException;

    Schedule getScheduleJobById(Integer scheduleId);

    Page<Schedule> getAllScheduleJobByPage(int pageIndex, int rows);

    List<Schedule> getAllScheduleJob();
}
