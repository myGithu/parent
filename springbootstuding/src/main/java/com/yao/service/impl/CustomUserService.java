package com.yao.service.impl;

import com.yao.dao.UsersMapper;
import com.yao.entity.Roles;
import com.yao.entity.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ruijie on 2018/2/11.
 */
@Transactional
@Service
public class CustomUserService implements UserDetailsService {

    @Autowired
    private UsersMapper usersMapper;

    @Override
    public UserDetails loadUserByUsername(String username) {
        Users user = usersMapper.getUserByName(username);
        if (user == null) {
            throw new UsernameNotFoundException("the userName not found");
        }
        return new User(user.getUserName(), "{bcrypt}" + new BCryptPasswordEncoder().encode(user.getPassword()), getUserAuthorityList(user));
    }

    public static List<GrantedAuthority> getUserAuthorityList(Users users) {
        List<Roles> roles = users.getRoles();
        List<GrantedAuthority> authorities = new ArrayList<>(roles.size());
        roles.forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getRoleName())));
        return authorities;
    }
}