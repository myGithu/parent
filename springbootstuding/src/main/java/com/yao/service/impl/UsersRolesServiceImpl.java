package com.yao.service.impl;

import com.yao.entity.UsersRoles;
import com.yao.dao.UsersRolesMapper;
import com.yao.service.IUsersRolesService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yao
 * @since 2018-02-15
 */
@Service
public class UsersRolesServiceImpl extends ServiceImpl<UsersRolesMapper, UsersRoles> implements IUsersRolesService {
	
}
