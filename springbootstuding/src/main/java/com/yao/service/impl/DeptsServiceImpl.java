package com.yao.service.impl;

import com.yao.entity.Depts;
import com.yao.dao.DeptsMapper;
import com.yao.service.IDeptsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yao
 * @since 2018-02-15
 */
@Service
public class DeptsServiceImpl extends ServiceImpl<DeptsMapper, Depts> implements IDeptsService {
	
}
