package com.yao.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.yao.entity.Users;
import com.yao.dao.UsersMapper;
import com.yao.service.IUsersService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author yao
 * @since 2018-01-31
 */
//@CacheConfig(cacheNames = "users")
@Transactional
@Service
public class UsersServiceImpl extends ServiceImpl<UsersMapper, Users> implements IUsersService {

    @Autowired
    private UsersMapper usersMapper;

    /**
     * 将方法的返回值存入缓存
     *
     * @param user
     * @return
     */
   // @CachePut(key = "#user.userNo")
    @Override
    public Users saveUsers(Users user) {
        Users users = queryUsersById(user.getUserNo());
        if(users == null){
            users = insert(user) ? user : null;
            System.out.println("saveUsershava为Id、key为：" + user.getUserNo() + "数据做了缓存");
            return users;
        }else{
            System.out.println("saveUserNoshava为Id为Id、key为：" + users.getUserNo() + "数据做了缓存");
            return users;
        }
    }

    /**
     * 根据value移除缓存
     *
     * @param userNo
     * @return
     */
    //@CacheEvict(value = "users")
    @Override
    public Boolean removeUsersById(String userNo) {
        System.out.println("removeUsersById删除了Id、key为：" + userNo + "数据做了缓存");

        return delete(new EntityWrapper<Users>().eq("userNo", userNo));
    }

    /**
     * 将方法的返回值存入缓存
     *
     * @param userNo
     * @return
     */
   // @Cacheable(key = "#userNo",sync = true)
    @Override
    public Users getUsersById(String userNo) {
        Users users = queryUsersById(userNo);
        System.out.println("getUsersById为Id、key为：" + userNo + "数据做了缓存");
        return users;
    }

    //@Cacheable(key = "#userName")
    @Override
    public Users getUserByName(String userName) {
        return usersMapper.getUserByName(userName);
    }


    private Users queryUsersById(String userNo){
        return selectOne(new EntityWrapper<Users>().eq("userNo", userNo));
    }


}
