package com.yao.service.impl;

import com.yao.entity.Roles;
import com.yao.dao.RolesMapper;
import com.yao.service.IRolesService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yao
 * @since 2018-02-15
 */
@Service
public class RolesServiceImpl extends ServiceImpl<RolesMapper, Roles> implements IRolesService {
	
}
