package com.yao.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.yao.entity.Schedule;
import com.yao.dao.ScheduleMapper;
import com.yao.service.IScheduleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yao.utils.ScheduleUtil;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yao
 * @since 2018-02-18
 */
@Transactional
@Service
public class ScheduleServiceImpl extends ServiceImpl<ScheduleMapper, Schedule> implements IScheduleService {

    @Autowired
    private Scheduler scheduler;

    @Override
    public Schedule addScheduleJob(Schedule schedule) throws SchedulerException {
        ScheduleUtil.createScheduleJob(scheduler,schedule);
        insert(schedule);
        return  schedule;
    }

    @Override
    public void startNowScheduleJob(Schedule schedule) throws SchedulerException {
        ScheduleUtil.startNowJob(scheduler,schedule.getJobName(),schedule.getJobGroup());
    }

    @Override
    public void stopScheduleJob(Schedule schedule) throws SchedulerException {
        ScheduleUtil.pauseJob(scheduler,schedule.getJobName(),schedule.getJobGroup());
        updateById(schedule);
    }

    @Override
    public void restartScheduleJob(Schedule schedule) throws SchedulerException {
        ScheduleUtil.resumeJob(scheduler,schedule.getJobName(),schedule.getJobGroup());
        updateById(schedule);
    }

    @Override
    public void modifyScheduleJob(Schedule schedule) throws SchedulerException {
        ScheduleUtil.updateScheduleJob(scheduler,schedule);
        updateById(schedule);
    }

    @Override
    public void removeScheduleJob(Schedule schedule) throws SchedulerException {
        ScheduleUtil.deleteScheduleJob(scheduler,schedule.getJobName(),schedule.getJobGroup());
        deleteById(schedule.getScheduleId());
    }

    @Override
    public Schedule getScheduleJobById(Integer scheduleId) {
        return selectById(scheduleId);
    }

    @Override
    public Page<Schedule> getAllScheduleJobByPage(int pageIndex, int rows) {
       return selectAllScheduleJobByPage(pageIndex,rows,new EntityWrapper<Schedule>());
    }

    @Override
    public List<Schedule> getAllScheduleJob() {
        return selectAllScheduleJob(new EntityWrapper<Schedule>());
    }

    private Page<Schedule> selectAllScheduleJobByPage(int pageIndex, int rows,Wrapper<Schedule> wrapper){
        Page<Schedule> page = new Page<>(pageIndex,rows);
        return selectPage(page,wrapper);
    }

    private List<Schedule> selectAllScheduleJob(Wrapper<Schedule> wrapper){
        return selectList(wrapper);
    }
}
