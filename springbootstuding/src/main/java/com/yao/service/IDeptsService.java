package com.yao.service;

import com.yao.entity.Depts;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yao
 * @since 2018-02-15
 */
public interface IDeptsService extends IService<Depts> {
	
}
