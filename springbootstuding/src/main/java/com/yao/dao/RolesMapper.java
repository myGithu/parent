package com.yao.dao;

import com.yao.entity.Roles;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author yao
 * @since 2018-02-15
 */
public interface RolesMapper extends BaseMapper<Roles> {

}