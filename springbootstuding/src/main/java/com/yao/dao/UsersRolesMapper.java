package com.yao.dao;

import com.yao.entity.UsersRoles;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author yao
 * @since 2018-02-15
 */
public interface UsersRolesMapper extends BaseMapper<UsersRoles> {

}