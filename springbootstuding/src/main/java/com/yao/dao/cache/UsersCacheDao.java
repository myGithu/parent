package com.yao.dao.cache;

import com.yao.entity.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

/**
 * Created by ruijie on 2018/2/3.
 */
@Repository
public class UsersCacheDao{

    @Autowired
    private RedisTemplate redisTemplate;

    public void save(Users users) {
       redisTemplate.opsForValue().set(users.getDeptNo(),users);
    }
}
