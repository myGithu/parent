package com.yao.dao;

import com.yao.entity.Schedule;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author yao
 * @since 2018-02-18
 */
public interface ScheduleMapper extends BaseMapper<Schedule> {

}