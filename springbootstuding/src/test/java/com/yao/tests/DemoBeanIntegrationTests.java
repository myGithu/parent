package com.yao.tests;

import com.yao.books.springbootaction.disanzhang.TestBean;
import com.yao.books.springbootaction.disanzhang.TestConfig;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by ruijie on 2017/12/28.
 */

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
@SpringBootTest
public class DemoBeanIntegrationTests {

        @Autowired
        private TestBean testBean;

        @Test
        public void prodBeanShouldInject(){
            String expected = "from producttion profile";
            String actual = testBean.getContent();
            Assert.assertEquals(expected,actual);
        }

}
