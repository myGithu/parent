package com.yao.tests;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.yao.core.util.DateTimeUtil;
import com.yao.dao.cache.UsersCacheDao;
import com.yao.entity.Schedule;
import com.yao.entity.Users;
import com.yao.service.IScheduleService;
import com.yao.service.IUsersService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTest {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private IUsersService usersService;

    @Autowired
    UsersCacheDao usersCacheDao;

    @Autowired
    IScheduleService iScheduleService;

    @Test
    public void test1() {
        stringRedisTemplate.opsForValue().set("aaa", "123456");
        Assert.assertEquals("123456", stringRedisTemplate.opsForValue().get("aaa"));
    }


    @Test
    public void testUsersService() {
        //usersService.saveUsers(new Users("dept0001", "user0001", "张三", "0", "18897487020", DateTimeUtil.getStringNowDateShort()));
    }

    @Test
    public void testGetUserById() {
        System.out.println(usersService.getUsersById("user0001"));
    }


    @Test
    public void testObj123() {
        //Users u = new Users("dept0001", "user0001", "张三", "0", "18897487020", DateTimeUtil.getStringNowDateShort());
        ValueOperations<String, Users> operations = redisTemplate.opsForValue();
        //operations.set("user.testuser",u);
        Users users = operations.get("user.testuser");
        System.out.println(users);

    }


    @Test
    public void testUsers(){
       // Users u = new Users("dept0002", "user0002", "李四", "1", "17621027403", DateTimeUtil.getStringNowDateShort());
       // usersCacheDao.save(u);
    }

    @Test
    public void testMp(){
      Users users =  new Users().selectOne(new EntityWrapper<Users>().eq("userName","张三"));
        System.out.println(users);
    }

    @Test
    public void testUserSecurity(){
        Users users = usersService.getUserByName("zs");
        System.out.println(users);
    }

    @Test
    public void testAddSchedule(){
        Schedule schedule = new Schedule("jobName", "jobGroup", "1/3 * * * * ?","0","description","com.yao.quartz.service.DoTaskScheduleService", "test");
        Schedule schedule1 = new Schedule("yao", "jobGroup", "1/10 * * * * ?","0","description123","com.yao.quartz.service.DoTaskScheduleService", "test1");
        try {
            iScheduleService.addScheduleJob(schedule);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }


}
